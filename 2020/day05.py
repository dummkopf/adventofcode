#!/usr/bin/env python

from lib.args import ArgumentParser


'''

'''

DAY_NUMBER = 5
SEAT_CODE_TRANSLATOR = str.maketrans('FBRL','0110')


def part1(input, logger, **__):
    '''
    As a sanity check, look through your list of boarding passes. What is the highest seat ID on a boarding pass?
    '''

    return _part1(load(input), logger=logger)

def _part1(passcodes, logger):
    return max(map(translate_code, passcodes))

def translate_code(passcode):
    return int(passcode.translate(SEAT_CODE_TRANSLATOR), 2)


def part2(input, logger, **__):
    '''
    Your seat wasn't at the very front or back, though; the seats with IDs +1 and -1 from yours will be in your list.

    What is the ID of your seat?
    '''

    return _part2(load(input), logger=logger)

def _part2(passcodes, logger):
    # looking for a single gap between seats

    sorted_ids = sorted(map(translate_code, passcodes))

    for a,b in zip(sorted_ids, sorted_ids[1:]):
        if b == a + 2:
            return a + 1
    

def load(input):
    with input.open() as f:
        for line in f:
            yield line.strip()


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
