#!/usr/bin/env python

from collections import deque
from itertools import combinations

from lib.args import ArgumentParser


'''

'''

DAY_NUMBER = 9


def part1(input, logger, **__):
    '''
    The first step of attacking the weakness in the XMAS data is to find the first number in the list (after the preamble) which is not the sum of two of the 25 numbers before it. What is the first number that does not have this property?
    '''

    return _part1(load(input), logger=logger)

def _part1(number_stream, logger):
    validator = XMASValidator(number_stream)

    assert not validator.valid

    return validator.error_value


def part2(input, logger, **__):
    '''
    
    '''

    return _part2(load(input), logger=logger)

def _part2(stuff, logger):
    pass
    

def load(input):
    with input.open() as f:
        yield from map(int, f)


class XMASValidator:
    def __init__(self, number_stream, preamble_length=25):
        self.number_stream = number_stream
        self.preamble_length = preamble_length 

    @property
    def valid(self):
        #load preamble (infallible)
        window = deque((v for _,v in zip(range(self.preamble_length), self.number_stream)), self.preamble_length)

        for i,v in enumerate(self.number_stream):
            if any(x+y==v for x,y in combinations(window, 2)):
                window.appendleft(v)
            else:
                self.error_value = v
                self.error_index = i + self.preamble_length
                return False

        return True


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
