#!/usr/bin/env python

from itertools import chain
import logging

from lib.args import ArgumentParser


'''

'''

DAY_NUMBER = 6


def part1(input, logger, **__):
    '''
    For each group, count the number of questions to which anyone answered "yes". What is the sum of those counts?
    '''

    return _part1(load(input), logger=logger)

def _part1(group_data, logger):
    if logger.getEffectiveLevel() is logging.DEBUG:
        count = 0

        for group in group_data:
            consolidated = set(chain.from_iterable(group))

            logger.debug(f'{len(consolidated)}: {sorted(consolidated)}')

            count += len(consolidated)

        return count
    else:
        return sum(len(set(chain.from_iterable(x))) for x in group_data)


def part2(input, logger, **__):
    '''
    For each group, count the number of questions to which everyone answered "yes". What is the sum of those counts?
    '''

    return _part2(load(input), logger=logger)

def _part2(group_data, logger):
    return sum(len(set.intersection(*map(set, x))) for x in group_data)
    

def load(input):
    current = []

    with input.open() as f:
        for line in f:
            line = line.strip()

            if line:
                current.append(line)
            else:
                yield current
                current = []

    if current:
        yield current


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
