#!/usr/bin/env python

from collections import Counter
from lib.args import ArgumentParser


'''
Before you leave, the Elves in accounting just need you to fix your expense report (your puzzle input); apparently, something isn't quite adding up.
'''

DAY_NUMBER = 1


def part1(input, **__):
    '''
    Specifically, they need you to find the two entries that sum to 2020 and then multiply those two numbers together.
    '''

    return _part1(load(input))

def _part1(entries, target=2020):
    entry_set = set(entries)

    #FIXME: missing edge case: what if entry and target are the same but it only appears in the input once?
    for entry in entry_set:
        if entry <= target // 2 and (target - entry) in entry_set:
            return entry * (target - entry)


def part2(input, logger, **__):
    '''
    '''

    return _part2(list(load(input)), logger)

def _part2(entries, logger, target=2020):
    entry_hist = Counter(entries)

    assert not any(v > 1 for v in entry_hist.values())

    logger.debug(f'scanning through {len(entry_hist)} entries')

    for i,first_entry in enumerate(entries):
        for second_entry in entries[i+1:]:
            attempt = target - first_entry - second_entry

            if attempt in entry_hist:
                return first_entry * second_entry * attempt


def load(input):
    with input.open() as f:
        for line in f:
            yield int(line.strip())


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
