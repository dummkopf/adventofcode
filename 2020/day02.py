#!/usr/bin/env python

from collections import Counter
from lib.args import ArgumentParser
import re


'''
To try to debug the problem, they have created a list (your puzzle input) of passwords (according to the corrupted database) and the corporate policy when that password was set.
'''

DAY_NUMBER = 2


def part1(input, **__):
    '''
    Each line gives the password policy and then the password. The password policy indicates the lowest and highest number of times a given letter must appear for the password to be valid. For example, 1-3 a means that the password must contain a at least 1 time and at most 3 times.
    '''

    return _part1(load(input))

def _part1(entries):
    return sum(1 for m,M,l,e in entries if m <= Counter(e)[l] <= M)


def part2(input, logger, **__):
    '''
    Each policy actually describes two positions in the password, where 1 means the first character, 2 means the second character, and so on. (Be careful; Toboggan Corporate Policies have no concept of "index zero"!) Exactly one of these positions must contain the given letter. Other occurrences of the letter are irrelevant for the purposes of policy enforcement.
    '''

    return _part2(load(input), logger)

def _part2(entries, logger):
    return sum(1 for m,M,l,e in entries if xor(e[m-1] == l, e[M-1] == l))
    

def load(input):
    return list(_load(input))

def _load(input):
    with input.open() as f:
        for line in f:
            match = re.match(r'(?P<min>\d+)\-(?P<max>\d+) (?P<letter>[a-z]): (?P<entry>[a-z]+)', line)

            if match:
                yield int(match.group('min')), int(match.group('max')), match.group('letter'), match.group('entry')
            else:
                print(f'WARNING: bad input line: {line!r}')

def xor(a, b):
    return (a and not b) or (b and not a)


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
