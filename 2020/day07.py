#!/usr/bin/env python

from collections import defaultdict
from functools import lru_cache
import re

from lib.args import ArgumentParser


'''

'''

DAY_NUMBER = 7


def part1(input, logger, **__):
    '''
    
    '''

    load(input)

    return _part1(logger=logger)

def _part1(logger):
    from pprint import pprint
    for name,irules in Rule.indexed_rules.items():
        print(f'===== {name} =====')
        pprint({k: list(map(str, v)) for k,v in irules.items()})

    roots = Rule.find_roots('shiny gold')
    logger.info('roots:\n{}'.format('\n'.join(f"  {root!s}" for root in roots)))

    return len(roots)


def part2(input, logger, **__):
    '''
    How many individual bags are required inside your single shiny gold bag?
    '''

    load(input)
    return _part2(logger=logger)

def _part2(logger):
    return Rule.indexed_rules['container']['shiny gold'][0].child_count - 1
    

def load(input):
    with input.open() as f:
        for line in f:
            Rule.parse(line.strip())


class Rule:
    INDEXES = {
        'container': lambda x: x.container_color,
        'contained': lambda x: list(x.contained_color_count_map.keys()),
    }

    all_rules = []
    indexed_rules = defaultdict(lambda: defaultdict(list))

    def __init__(self, container, contained):
        self.container_color = container
        self.contained_color_count_map = contained

    def __str__(self):
        return f'<{self.__class__.__name__} {self.container_color} -> [{", ".join(f"{v}x {k}" for k,v in self.contained_color_count_map.items())}]>'

    @classmethod
    def parse(cls, raw):
        match = re.fullmatch(r'(?P<container>[a-z ]+?) bags contain (?P<contained_section>.+?)\.', raw)

        if match:
            raw_contained = match.group('contained_section').split(', ')

            if raw_contained == ['no other bags']:
                contained = []
            else:
                contained = [re.fullmatch(r'(?P<count>\d+) (?P<color>[a-z ]+?) bags?', raw).groupdict() for raw in raw_contained]

            to_return = cls(match.group('container'), {x['color']: int(x['count']) for x in contained})

            cls.all_rules.append(to_return)
            cls.index_all(to_return)

            return to_return
        else:
            raise BadLine(raw)

    @classmethod
    def index_all(cls, rule):
        for name, pred in cls.INDEXES.items():
            cls.index(name, pred, rule)

    @classmethod
    def index(cls, name, pred, rule):
        pred_values = pred(rule)

        if isinstance(pred_values, list):
            for pred_value in pred_values:
                cls.indexed_rules[name][pred_value].append(rule)
        else:
            cls.indexed_rules[name][pred_values].append(rule)

    @classmethod
    def find_roots(cls, color, found=None):
        # use "contained" index to traverse toward all roots
        # track found ancestors to avoid loops

        found = found or set()
        fresh = cls.find_parents(color) - found

        for parent in fresh:
            found |= cls.find_roots(parent.container_color, found)

        return found | fresh

    @classmethod
    def find_parents(cls, color):
        return set(cls.indexed_rules['contained'][color])

    @property
    @lru_cache
    def child_count(self):
        return 1 + sum(count*self.indexed_rules['container'][color][0].child_count for color,count in self.contained_color_count_map.items())
    


class BadLine(Exception): pass


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
