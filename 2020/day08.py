#!/usr/bin/env python

from lib.args import ArgumentParser


'''

'''

DAY_NUMBER = 8


def part1(input, logger, **__):
    '''
    Run your copy of the boot code. Immediately before any instruction is executed a second time, what value is in the accumulator?
    '''

    return _part1(load(input), logger=logger)

def _part1(machine, logger):
    machine.execute_until_loop()

    return machine.accumulator


def part2(input, logger, **__):
    '''
    Fix the program so that it terminates normally by changing exactly one jmp (to nop) or nop (to jmp). What is the value of the accumulator after the program terminates?
    '''

    return _part2(load(input), logger=logger)

def _part2(machine, logger):
    for i,op in enumerate(machine.instructions):
        if isinstance(op, jump):
            replacement = noop(op.arg)
        elif isinstance(op, noop):
            replacement = jump(op.arg)
        else:
            replacement = None

        if replacement:
            logger.debug(f'replacing {op!s} with {replacement!s} at {i}')
            doppel = machine.fresh_copy()
            doppel.instructions[i] = replacement

            doppel.execute_until_loop()
            logger.debug(f'reached pc {doppel.pc}')

            if doppel.reached_end:
                return doppel.accumulator

def load(input):
    return Machine(_load(input))

def _load(input):
    with input.open() as f:
        for line in f:
            code, argstr = line.strip().split(' ')
            arg = int(argstr)

            yield INSTRUCTIONS[code](arg)


class Machine:
    def __init__(self, instructions, initial_pc=0):
        self.pc = initial_pc
        self.instructions = list(instructions)
        self.accumulator = 0

    def __len__(self):
        return len(self.instructions)

    def execute_one(self):
        self.current(self)

    def execute_until_loop(self):
        while self.pc < len(self.instructions) and not self.current.call_count:
            self.execute_one()

    def fresh_copy(self):
        return self.__class__(x.fresh_copy() for x in self.instructions)

    @property
    def current(self):
        return self.instructions[self.pc]

    @property
    def reached_end(self):
        return self.pc >= len(self.instructions)
    

class Instruction:
    def __init__(self, arg):
        self.arg = arg
        self.call_count = 0

    def __call__(self, machine):
        self.precall(machine)
        self.guts(machine)
        self.postcall(machine)

    def __str__(self):
        return f'Instruction {self.__class__.__name__}({self.arg})'

    def fresh_copy(self):
        return self.__class__(self.arg)

    def precall(self, machine):
        self.call_count += 1
        # print(f'CALL {self!s} at pc {machine.pc}')

    def postcall(self, machine):
        machine.pc += 1
        # print(f'DONE: pc: {machine.pc}, acc: {machine.accumulator}')

    def guts(self, machine):
        pass

class accumulate(Instruction):
    def __init__(self, arg):
        super().__init__(arg)

    def guts(self, machine):
        machine.accumulator += self.arg

class jump(Instruction):
    def __init__(self, arg):
        super().__init__(arg)

    def guts(self, machine):
        machine.pc += self.arg - 1

class noop(Instruction):
    pass

INSTRUCTIONS = {
    'acc': accumulate,
    'jmp': jump,
    'nop': noop,
}


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
