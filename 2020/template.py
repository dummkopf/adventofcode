#!/usr/bin/env python

from lib.args import ArgumentParser


'''

'''

DAY_NUMBER = ???


def part1(input, logger, **__):
    '''
    
    '''

    return _part1(load(input), logger=logger)

def _part1(stuff, logger):
    pass


def part2(input, logger, **__):
    '''
    
    '''

    return _part2(load(input), logger=logger)

def _part2(stuff, logger):
    pass
    

def load(input):
    with input.open() as f:
        for line in f:
            pass


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
