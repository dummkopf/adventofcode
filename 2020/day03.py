#!/usr/bin/env python

from math import ceil

from lib.args import ArgumentParser


'''

'''

DAY_NUMBER = 3


def part1(input, logger, **__):
    '''
    Starting at the top-left corner of your map and following a slope of right 3 and down 1, how many trees would you encounter?
    '''

    return _part1(*load(input), logger=logger)

def _part1(height, themap, logger):
    return count_trees(themap, height, (3,1))


def part2(input, logger, **__):
    '''
    What do you get if you multiply together the number of trees encountered on each of the listed slopes?
    '''

    return _part2(*load(input), logger=logger)

def _part2(height, themap, logger):
    return (
        count_trees(themap, height, (1,1)) *
        count_trees(themap, height, (3,1)) *
        count_trees(themap, height, (5,1)) *
        count_trees(themap, height, (7,1)) *
        count_trees(themap, height, (1,2))
    )
    

def load(input):
    themap = None

    with input.open() as f:
        for y,line in enumerate(f):
            line = line.strip()

            if themap is None:
                themap = WraparoundDict(len(line))

            themap.update(((x,y),l == '#') for x,l in enumerate(line))

    return y+1, themap


def count_trees(themap, height, slope):
    (slopex,slopey) = slope

    return sum(themap[slopex*i, slopey*i] for i in range(int(ceil(height / slopey))))


class WraparoundDict(dict):
    def __init__(self, width, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.width = width

    def __getitem__(self, key):
        x,y = key

        return super().__getitem__((x%self.width, y))


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
