#!/usr/bin/env python

import re

from lib.args import ArgumentParser


'''

'''

DAY_NUMBER = 4

REQUIRED_FIELDS = {
    'byr',
    'iyr',
    'eyr',
    'hgt',
    'hcl',
    'ecl',
    'pid',
}

def validate_height(value):
    match = re.fullmatch(r'(\d+)(in|cm)', value)

    if match:
        number, unit = match.groups()

        if unit == 'cm':
            return 150 <= int(number) <= 193
        elif unit == 'in':
            return 59 <= int(number) <= 76
        else:
            raise Exception('hwat')

FIELD_RULES = {
    'byr': lambda v: len(v) == 4 and v.isnumeric() and 1920 <= int(v) <= 2002,
    'iyr': lambda v: len(v) == 4 and v.isnumeric() and 2010 <= int(v) <= 2020,
    'eyr': lambda v: len(v) == 4 and v.isnumeric() and 2020 <= int(v) <= 2030,
    'hgt': validate_height,
    'hcl': lambda v: re.fullmatch(r'#[0-9a-f]{6}', v),
    'ecl': lambda v: v in {'amb','blu','brn','gry','grn','hzl','oth'},
    'pid': lambda v: re.fullmatch(r'\d{9}', v),
}


def part1(input, logger, **__):
    '''
    Count the number of valid passports - those that have all required fields. Treat cid as optional. In your batch file, how many passports are valid?
    '''

    return _part1(load(input), logger=logger)

def _part1(records, logger):
    return sum(map(validate1, records))

def validate1(record):
    return not (REQUIRED_FIELDS - record.keys())


def part2(input, logger, **__):
    '''
    
    '''

    return _part2(load(input), logger=logger)

def _part2(records, logger):
    return sum(validate1(r) and validate2(r) for r in records)

def validate2(record):
    return all(bool(FIELD_RULES.get(field, lambda v: True)(value)) for field,value in record.items())
    

def load(input):
    return list(_load(input))

def _load(input):
    current = {}

    with input.open() as f:
        for line in f:
            if line.strip():
                rawfields = line.strip().split(' ')
                current.update(x.split(':') for x in rawfields)
            else:
                yield current
                current = {}

    if current:
        yield current


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
