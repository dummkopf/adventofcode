from alumium.lib import logging
from alumium.solutions.y2022.d12 import Part1, Part2


logging.required_level = -1


def sample1():
	subject = Part1.with_sample_input()
	subject.run()

	logging.log(lambda: subject.winning_path.visual)

	assert subject.answer == 31

def sample2():
	subject = Part2.with_sample_input()
	subject.run()
	
	logging.log(lambda: subject.winning_path.visual)

	assert subject.answer == 0
