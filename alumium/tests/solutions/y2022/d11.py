from alumium.solutions.y2022.d11 import Part1, Part2


def sample1():
	subject = Part1.with_sample_input()
	subject.run()

	assert subject.answer == 10605

def sample2():
	subject = Part2.with_sample_input()
	subject.run()

	assert subject.answer == 2713310158
