from io import StringIO

import pytest

from alumium.lib.pushback import PushbackBuffer


@pytest.fixture
def sample_stream():
	return StringIO('first line\nsecond line')


class x__next__:
	def normal(self, sample_stream):
		subject = PushbackBuffer(sample_stream)

		assert next(subject) == 'first line'
		assert next(subject) == 'second line'

		try:
			next(subject)
		except StopIteration:
			pytest.xfail('EOF')

	def after_pushback(self, sample_stream):
		subject = PushbackBuffer(sample_stream)

		subject.pushback('zeroth line')

		assert next(subject) == 'zeroth line'
		assert next(subject) == 'first line'
		assert next(subject) == 'second line'

		try:
			next(subject)
		except StopIteration:
			pytest.xfail('EOF')

class pushback:
	def one(self, sample_stream):
		subject = PushbackBuffer(sample_stream)

		subject.pushback('new line')

		assert next(subject) == 'new line'
		assert next(subject) == 'first line'

	def few(self, sample_stream):
		subject = PushbackBuffer(sample_stream)

		subject.pushback('a', 'b', 'c')

		assert next(subject) == 'a'
		assert next(subject) == 'b'
		assert next(subject) == 'c'
		assert next(subject) == 'first line'

class context:
	@pytest.mark.todo
	def none(self, sample_stream):
		pass

	@pytest.mark.todo
	def one(self, sample_stream):
		pass

	@pytest.mark.todo
	def two(self, sample_stream):
		pass
