import pytest

from alumium.lib.lists.doubly_linked_list import Node, BaseNode


class TestBaseNode:
	@pytest.mark.skip('perfunctory')
	class stitch:
		pass

	class insert:
		def bare(self):
			node1 = BaseNode(1)
			node2 = BaseNode(2)
			node3 = BaseNode(3)

			assert BaseNode.insert(node2, node1, node3) is None

			assert node1.next is node2
			assert node2.prev is node1
			assert node2.next is node3
			assert node3.prev is node2

class TestNode:
	class join:
		def singles(self):
			root1 = Node(1)
			root2 = Node(2)

			assert Node.join(root1, root2) is None

			assert root1.next is root2
			assert root2.prev is root1

		def doubles(self):
			root1 = Node(1)
			end1 = Node(1.1)
			root1.next, end1.prev = end1, root1
			root2 = Node(2)
			end2 = Node(2.1)
			root2.next, end2.prev = end2, root2

			assert Node.join(root1, root2) is None

			assert end1.next is root2
			assert root2.prev is end1

	class append:
		def singles(self):
			root1 = Node(1)
			root2 = Node(2)

			assert root1.append(root2) is None

			assert root1.next is root2
			assert root2.prev is root1

		def doubles(self):
			root1 = Node(1)
			end1 = Node(1.1)
			root1.next, end1.prev = end1, root1
			root2 = Node(2)
			end2 = Node(2.1)
			root2.next, end2.prev = end2, root2

			assert root1.append(root2) is None

			assert root1.next is root2
			assert root2.prev is root1

			assert end2.next is end1
			assert end1.prev is end2

		def doubles_end(self):
			root1 = Node(1)
			end1 = Node(1.1)
			root1.next, end1.prev = end1, root1
			root2 = Node(2)
			end2 = Node(2.1)
			root2.next, end2.prev = end2, root2

			assert root1.append(root2, end=True) is None

			assert end1.next is root2
			assert root2.prev is end1

	@pytest.mark.skip('just lazy')
	class prepend:
		pass

	class pop:
		def single(self):
			root = Node(1)

			assert root.pop() is None

		def double_from_end(self):
			root = Node(1)
			end = Node(2)
			root.next = end

			assert end.pop() is None

		def double_from_root(self):
			root = Node(1)
			end = Node(2)
			root.next = end

			assert root.pop() is end

		def double_end(self):
			root = Node(1)
			end = Node(2)
			root.next, end.prev = end, root

			assert root.pop(end=True) is end

	@pytest.mark.skip('just lazy')
	class lpop:
		pass

	class last:
		def single(self):
			root = Node(1)

			assert root.last is root

		def double(self):
			root = Node(1)
			end = Node(2)
			root.next = end

			assert root.last is end

	class first:
		def single(self):
			root = Node(1)

			assert root.first is root

		def double(self):
			root = Node(1)
			end = Node(2)
			end.prev = root

			assert end.first is root
