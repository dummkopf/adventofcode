import pytest

from alumium.lib.lists.circular_linked_list import Node
from alumium.lib.lists.doubly_linked_list import Node as DLL


class as_dot:
	def only(self):
		node = Node.as_dot(1)

		assert node.next is node
		assert node.prev is node

class from_linear:
	def single(self):
		root = DLL(1)

		assert Node.from_linear(root) is None

		assert root.prev is root
		assert root.next is root

	def double_from_root(self):
		root = DLL(1)
		end = DLL(2)
		root.next, end.prev = end, root

		assert Node.from_linear(root) is None

		assert root.prev is end
		assert end.next is root

	def double_from_end(self):
		root = DLL(1)
		end = DLL(2)
		root.next, end.prev = end, root

		assert Node.from_linear(end) is None

		assert root.prev is end
		assert end.next is root

@pytest.mark.skip('just lazy')
class append:
	pass

@pytest.mark.skip('just lazy')
class prepend:
	pass
