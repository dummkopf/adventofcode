import pytest

from alumium.lib.lists.singly_linked_list import Node


class append:
	def onto_single(self):
		root = Node(1)
		root.append((new := Node(2)))

		assert root.next is new
		assert root.next.value == 2

	def onto_filled(self):
		root = Node(1)

		root.append((last := Node(2)))
		root.append((middle := Node(3)))

		assert root.next is middle
		assert middle.next is last

	def onto_end_of_filled(self):
		root = Node(1)

		root.append((last := Node(2)))
		root.append((middle := Node(3)), end=True)

		assert root.next is last
		assert last.next is middle

	def list_onto_filled(self):
		root1 = Node(1)
		root2 = Node(3)

		root1.append((end1 := Node(2)))
		root2.append((end2 := Node(4)))

		root1.append(root2)

		assert root1.next is root2
		assert root2.next is end2
		assert end2.next is end1

@pytest.mark.skip("This is just sugar so eh.")
class prepend:
	pass

class pop:
	def single(self):
		root = Node(1)
		actual = root.pop()

		assert actual is None

	def with_tail(self):
		root = Node(1)
		root.append((end := Node(2)))
		actual = root.pop()

		assert actual is end

	def with_long_tail(self):
		root = Node(1)

		root.append((middle := Node(2)))
		middle.append((end := Node(3)))

		actual = root.pop()

		assert actual is middle
		assert root.next is end

class last:
	def single(self):
		root = Node(1)
		actual = root.last

		assert actual is root

	def two(self):
		root = Node(1)

		root.append((end := Node(2)))

		actual = root.last

		assert actual is end

	def four(self):
		root = Node(1)
		root.append(Node(2))
		root.append(Node(3), end=True)
		root.append((end := Node(4)), end=True)

		actual = root.last

		assert actual is end
