from functools import lru_cache
from math import prod
from typing import ForwardRef, Callable
import operator as op
import re

from attrs import mutable, field

from alumium.lib.parse import InputFile, Section, Record
from alumium.lib.solution import Solution
from alumium.lib.logging import log


class Day11(Solution):
	'''
	Monkeys are playing Keep Away with your missing things!

	To get your stuff back, you need to be able to predict where the monkeys will throw your items.
	After some careful observation, you realize the monkeys operate based on how worried you are
	about each item.

	The monkeys take turns inspecting and throwing items.
	On a single monkey's turn, it inspects and throws all of the items it is
	holding one at a time and in the order listed.
	The process of each monkey taking a single turn is called a round.

	When a monkey throws an item to another monkey, the item goes on the end of
	the recipient monkey's list. A monkey that starts a round with no items
	could end up inspecting and throwing many items by the time its turn comes
	around. If a monkey is holding no items at the start of its turn, its turn ends.
	'''

	YEAR = 2022
	DAY = 11
	INPUT_CLS = ForwardRef('MonkeysInput')

	@property
	@lru_cache
	def monkeys(self):
		return self.input_file.collections[0].records

	def run(self):
		for r in range(self.ROUNDS):
			self.run_round()

	def run_round(self):
		for monkey in self.monkeys:
			monkey.throw_all(self.monkeys, self.relieve)

	def relieve(self, item, monkey):
		raise NotIpmlementedError

	@property
	def answer(self):
		most_active = sorted(self.monkeys, key=lambda x: x.activity_count, reverse=True)[:2]

		return prod(x.activity_count for x in most_active)


class Part1(Day11):
	'''
	After each monkey inspects an item but before it tests your worry level,
	your relief that the monkey's inspection didn't damage the item causes
	your worry level to be divided by three and rounded down to the nearest integer.

	Figure out which monkeys to chase by counting how many items they inspect over 20 rounds.
	What is the level of monkey business after 20 rounds of stuff-slinging simian shenanigans?
	'''

	PART = 1
	ROUNDS = 20
	REPORT_INTERVAL = 1

	def relieve(self, item, monkey):
		return item // 3


class Part2(Day11):
	'''
	Worry levels are no longer divided by three after each item is inspected;
	you'll need to find another way to keep your worry levels manageable.
	Starting again from the initial state in your puzzle input,
	what is the level of monkey business after 10000 rounds?
	'''

	PART = 2
	ROUNDS = 10000
	REPORT_INTERVAL = 1000

	monkey_ring_size:int

	def run(self):
		self.monkey_ring_size = prod(x.divisible_by for x in self.monkeys)

		super().run()

	def relieve(self, item, monkey):
		# modulo translation is apparently hard
		# however, a ≡ b(mod m) ⟹ a/n ≡ b/n(mod m/n), where a,b,m,n,a/n,b/n,m/n ∈ N
		# we can exploit this by taking modulo of the lcm of all the monkeys' divisors
		# conveniently, the divisors are all prime numbers so that's just the product

		return item % self.monkey_ring_size


@mutable(kw_only=True, slots=False)
class Monkey(Record):
	index:int = None
	items:tuple[int,int] = None
	operator:Callable[[int,int],int] = None
	operand:int = None
	divisible_by:int = None
	pass_dest:int = None
	fail_dest:int = None

	activity_count:int = 0

	def parse(self):
		with self.buffer:
			self.index, = self.chomp(r'Monkey (\d+):', int)
			self.items = self.chomp_list(r'  Starting items: (\d+(?:, \d+)*)', int)
			self.operator, self.operand = self.munge_operation(
				*self.chomp(r'  Operation: new = old ([*+\-]) (\d+|old)')
			)
			self.divisible_by, = self.chomp(r'  Test: divisible by (\d+)', int)
			self.pass_dest, = self.chomp(r'    If true: throw to monkey (\d+)', int)
			self.fail_dest, = self.chomp(r'    If false: throw to monkey (\d+)', int)

		self.chuck_empty(all=True)

	@classmethod
	def munge_operation(cls, raw_operator, raw_operand):
		if raw_operand == 'old':
			if raw_operator == '*':
				return op.pow, 2
			elif raw_operator == '+':
				return op.mul, 2
			elif raw_operator == '-':
				return op.mul, 0
		else:
			if raw_operator == '*':
				return op.mul, int(raw_operand)
			elif raw_operator == '+':
				return op.add, int(raw_operand)
			elif raw_operator == '-':
				return op.sub, int(raw_operand)

		raise ValueError(f'can\'t handle new = old {raw_operator} {raw_operand}')

	def throw_all(self, collection, relieve):
		for item in self.items:
			item = self.inspect(item)
			item = relieve(item, self)

			if item % self.divisible_by == 0:
				destination = self.pass_dest
			else:
				destination = self.fail_dest

			collection[destination].items.append(item)

		self.activity_count += len(self.items)
		self.items.clear()

	def inspect(self, item):
		return self.operator(item, self.operand)


@mutable(kw_only=True, slots=False)
class MonkeySection(Section):
	RECORD_CLS = Monkey


@mutable(kw_only=True)
class MonkeysInput(InputFile):
	SECTIONS = [MonkeySection]
