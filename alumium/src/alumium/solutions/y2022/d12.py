from functools import lru_cache
from itertools import count, zip_longest
from math import prod
from typing import ForwardRef, Callable, MutableMapping, ClassVar, Sequence
import operator as op
import re

from attrs import mutable, frozen, field, evolve

from alumium.lib.parse import InputFile, Section, Record
from alumium.lib.solution import Solution
from alumium.lib.logging import log


class Day11(Solution):
	'''
	You ask the device for a heightmap of the surrounding area (your puzzle input).
	The heightmap shows the local area from above broken into a grid; the elevation
	of each square of the grid is given by a single lowercase letter, where a is
	the lowest elevation, b is the next-lowest, and so on up to the highest elevation, z.

	Also included on the heightmap are marks for your current position (S) and the
	location that should get the best signal (E). Your current position (S) has
	elevation a, and the location that should get the best signal (E) has elevation z.
	'''

	YEAR = 2022
	DAY = 12
	INPUT_CLS = ForwardRef('HeightmapInput')

	winning_path:'Path'

	@property
	@lru_cache
	def heightmap(self):
		return self.input_file.collections[0].records[0]

	def run(self):
		self.winning_path = next(self.heightmap.root_path.walk())

	@property
	def answer(self):
		return len(self.winning_path)


class Part1(Day11):
	'''
	You'd like to reach E, but to save energy, you should do it in as few steps as
	possible. During each step, you can move exactly one square up, down, left, or
	right. To avoid needing to get out your climbing gear, the elevation of the
	destination square can be at most one higher than the elevation of your current
	square; that is, if your current elevation is m, you could step to elevation n,
	but not to elevation o. (This also means that the elevation of the destination
	square can be much lower than the elevation of your current square.)
	'''

	PART = 1


class Part2(Day11):
	'''
	'''

	PART = 2


Coord = tuple[int,int]


@mutable(kw_only=True, slots=False)
class Heightmap(Record):
	heights:MutableMapping[Coord, int] = field(init=False, factory=dict)
	start:Coord = None
	end:Coord = None

	HEIGHT_CHR_INT_MAP:ClassVar = {chr(x+ord('a')): x for x in range(26)} | {'S': 0, 'E': 25}

	def parse(self):
		for y in count():
			try:
				line, = self.chomp(r'(.+)')
			except StopIteration:
				return

			for x,c in enumerate(line):
				self.heights[x,y] = self.HEIGHT_CHR_INT_MAP[c]

				if c == 'S':
					self.start = (x,y)
				elif c == 'E':
					self.end = (x,y)

	@property
	def root_path(self):
		return Path(heightmap=self, current=self.start)

	def __getitem__(self, key):
		return self.heights[key]


@frozen
class Path:
	heightmap:Heightmap = field(repr=False)
	current:Coord
	history:Sequence[Coord] = field(factory=list)
	visited:set[Coord] = field(factory=set)

	def __len__(self):
		return len(self.history)

	def walk(self):
		'''
		Navigate available paths until an answer is reached.
		The shortest path is what's asked for, so optimality (and thus exhaustiveness) required.
		Probably O(N^2)
		'''

		###IDEA: cut N in half by seeking backward at the same time
		#        not sure about optimality

		log(f'{" "*len(self)}walking into {self.current}', -2)

		self.visited.add(self.current)

		if self.success:
			# if we've arrived, we win
			# I think the first time we have a success, it will be the optimal solution
			# due to the sorting done in multiply()
			log(f'{" "*len(self)} success', -2)

			yield self
		else:
			# if we arrive at a dead end, there will be no available neighbors
			# thus multiply() will return empty and there will be no yield here

			for subpath in (subpaths := self.multiply()):
				yield from subpath.walk()

			if not subpaths:
				log(lambda: self.visual + '\n', -1)
				log(f'{" "*len(self)} dead', -2)


	def multiply(self):
		'''
		Return new `Path`s that increment to the next available coords.
		Sorted in descending fitness.
		'''

		return sorted(
			map(self.move, self.neighborhood),
			key=lambda x: x.fitness,
		)

	def move(self, new_coord:Coord):
		return evolve(
			self,
			current=new_coord,
			history=self.history + [self.current],
			visited=self.visited,
		)

	@property
	def neighborhood(self):
		raw = (tuple(map(sum, zip(self.current, p))) for p in [(1,0),(0,1),(-1,0),(0,-1)])
		good = {
			r for r in raw if (
				self.in_bounds(r) and
				self.reachable(r) and
				r not in self.visited
			)
		}

		return good

	@property
	def fitness(self):
		'''
		Manhattan distance of current to target, including height.
		Less is better.
		'''

		x_distance = abs(self.current[0] - self.heightmap.end[0])
		y_distance = abs(self.current[1] - self.heightmap.end[1])
		z_distance = abs(self.heightmap[self.current] - self.heightmap[self.heightmap.end])

		return x_distance + y_distance + z_distance

	@property
	def success(self):
		return self.fitness == 0

	def in_bounds(self, point):
		return point in self.heightmap.heights

	def reachable(self, point):
		height_increase = self.heightmap[point] - self.heightmap[self.current]

		return height_increase < 2

	@property
	def visual(self):
		symbols = {
			(1,0): '>',
			(-1,0): '<',
			(0,1): 'v',
			(0,-1): '^',
		}

		fullhist = self.history + [self.current]
		hsize = max(x for x,y in fullhist) + 1
		vsize = max(y for x,y in fullhist) + 1
		grid = [['.']*hsize for _ in range(vsize)]

		for before,after in zip_longest(fullhist, fullhist[1:]):
			if after:
				direction = (
					after[0]-before[0],
					after[1]-before[1],
				)
			else:
				direction = None

			grid[before[1]][before[0]] = symbols.get(direction, 'E')

		return '\n'.join(''.join(line) for line in grid)


@frozen(kw_only=True, slots=False)
class OnlySection(Section):
	RECORD_CLS = Heightmap

	def parse(self):
		self.parse_one()


@mutable(kw_only=True)
class HeightmapInput(InputFile):
	SECTIONS = [OnlySection]
