from typing import Any, Self, Callable

from attrs import mutable


@mutable
class BaseNode:
	value:Any
	next:Self|None = None
	prev:Self|None = None

	def _seek(self, until:Callable[[Self], bool], advance:Callable[[Self], Self]) -> Self|None:
		current = self

		while current:
			if until(current):
				return current

			current = advance(current)

		return None

	@classmethod
	def stitch(cls, before:Self, after:Self):
		before.next, after.prev = after, before

	@classmethod
	def insert(cls, new:Self, before:Self, after:Self):
		'''
		Destructively insert a new node `new` between nodes `before` and `after`.
		"Destructive" because `new` will have its next and prev overwritten,
		potentially orphaning anything remaining part of its list.
		'''

		cls.stitch(before, new)
		cls.stitch(new, after)


@mutable
class Node(BaseNode):
	@classmethod
	def _join(cls, first:Self|None, second:Self|None):
		'''
		Fuse two lists together and return the intervening list (see below).
		If `None` is passed as either list, elide instead of join, on appropriate end.
		Used internally for all operations except seek.

		The "intervening list" is the joined excess that is otherwise removed.
		For instance,
			given list A composed of a,b,c
			      list B composed of d,e,f

			joining b and e results in list C composed of a,b,e,f and the excess of c,d
		'''

		first_excess = first and first.next
		second_excess = second and second.prev and second.first

		if first:
			first.next = second

		if second:
			second.prev = first

		if first_excess:
			if second_excess:
				return cls._join(first_excess, second_excess)
			else:
				first_excess.next = None

				return first_excess
		else:
			if second_excess:
				second_excess.prev = None

				return second_excess
			else:
				return None

	@classmethod
	def join(cls, first:Self, second:Self):
		'''
		Merge two lists.
		'''

		# prevent removal behavior

		cls._join(first.last, second.first)

	def append(self, new:Self, end=False):
		'''
		Add a node/list to the end of this one.
		If there is already a next, insert after this node.
		If `end` passed, seek to the end, then append.

		Can be used to append-list and insert-after.
		'''

		if end:
			self.last.append(new)
		else:
			excess = self._join(self, new)
			self._join(self.last, excess)

	def prepend(self, new:Self, end=False):
		'''
		Add a node/list to the beginning of this one.
		If there is already a prev, insert before this node.
		If `end` passed, seek to the end, then prepend.

		Can be used to prepend-list and insert-before.
		'''

		if end:
			self.first.append(new)
		else:
			excess = self._join(new, self)
			self._join(excess, self.last)

	def pop(self, end=False):
		'''
		Remove and return the next node.
		If `end`, remove and return the last node.
		'''

		if end:
			return self._join(self.last.prev, None)
		else:
			return self._join(self, self.next and self.next.next)

	def lpop(self, end=False):
		'''
		Remove and return the previous node.
		If `end`, remove and return the first node.
		'''

		if end:
			return self._join(None, self.first.next)
		else:
			return self._join(self.prev and self.prev.prev, self)

	@property
	def last(self):
		return self._seek(until=lambda x: not x.next, advance=lambda x: x.next)

	@property
	def first(self):
		return self._seek(until=lambda x: not x.prev, advance=lambda x: x.prev)
