from typing import Any, Self

from attrs import mutable

from .doubly_linked_list import BaseNode, Node as DoublyLinkedList


@mutable
class Node(BaseNode):
	@classmethod
	def as_dot(cls, value:Any):
		'''
		Initialize a minimal-size circular list.
		'''

		to_return = cls(value)
		to_return.prev = to_return
		to_return.next = to_return

		return to_return

	@classmethod
	def from_linear(cls, linlist:DoublyLinkedList):
		'''
		Convert a DLL to a CDLL.
		'''

		end = linlist.last
		first = linlist.first

		end.next, first.prev = first, end

	def append(self, new:Self):
		'''
		Insert a new node after this one.
		'''

		self.insert(new, self, self.next)

	def prepend(self, new:Self):
		'''
		Insert a new node before this one.
		'''

		self.insert(new, self.prev, self)

