from typing import Any, Self

from attrs import mutable


@mutable
class Node:
	value:Any
	next:Self|None = None

	def append(self, new:Self, end=False):
		'''
		Add a node/list to the end of this one.
		If there is already a next, insert after this node.
		If `end` passed, seek to the end, then append.

		Can be used to append, insert, and join.
		'''

		if end:
			self.last.append(new)
		else:
			old_next, self.next = self.next, new

			if old_next:
				self.last.append(old_next)

	def prepend(self, new:Self, end=False):
		if end:
			new = new.last

		new.append(self)

	def pop(self):
		'''
		Remove and return the next node.
		'''

		if (tomorrow := self.next):
			self.next = tomorrow.next
			tomorrow.next = None

		return tomorrow

	@property
	def last(self):
		current = self

		while (next := current.next):
			current = next

		return current

