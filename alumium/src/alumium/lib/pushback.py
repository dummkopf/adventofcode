from itertools import chain
from typing import Iterator

from attrs import mutable, field


@mutable
class PushbackBuffer:
	buffer:Iterator
	queue:list[str]|None = field(init=False, default=None)

	def __iter__(self):
		return self

	def __next__(self):
		text = next(self.buffer)  # can raise StopIteration

		if not text:
			raise StopIteration

		if self.queue:
			self.queue.append(text)

		return text.rstrip('\n')

	def __enter__(self):
		self.queue = []

		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		if exc_value:
			self.pushback(*self.queue)  #type:ignore

		self.queue = None

	def pushback(self, *lines):
		self.buffer = chain(lines, self.buffer)

	def close(self):
		self.buffer.close()  #type:ignore
