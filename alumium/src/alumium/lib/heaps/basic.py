from collections.abc import Iterable
from copy import copy

from attrs import mutable, field

from ..trees.basic import Node as TreeNode


@mutable(slots=False, kw_only=True)
class Heap:
	root:'Node'|None

	@classmethod
	def from_iterable(cls, iterable:Iterable) -> Self:
		'''
		aka heapify
		'''

		raise NotImplementedError

	@classmethod
	def merge(cls, *origs:Self) -> Self:
		'''
		Create new heap with the contents of provided heaps.
		Non-destructive.
		'''

		dupe_start, *dupe_news = [copy(x) for x in origs]

		dupe_start.meld(*dupe_news)

		return dupe_start

	def meld(self, *news:Self):
		'''
		Create new heap with the contents of provided heaps.
		Destructive.
		'''

		raise NotImplementedError

	def __len__(self):
		raise NotImplementedError

	def __bool__(self):
		return bool(len(self))

	def peek(self) -> Self:
		return self._seek(until=lambda x: not x.parent, advance=lambda x: x.parent)

	def push(self) -> None:
		raise NotImplementedError

	def pop(self) -> Self:
		raise NotImplementedError

	def prop(self, new:Self):
		'''
		aka replace: pop + push, but more efficient
		'''

		raise NotImplementedError


@mutable(slots=False, kw_only=True)
class Node(TreeNode):
	tree:Heap
	children:set[Self] = field(factory=set)

	@property
	def correct(self):
		pass

	@property
	def key(self):
		return self.value

	def _seek(self, until:Callable[[Self], bool], advance:Callable[[Self], Self]) -> Self|None:
		current = self

		while current:
			if until(current):
				return current

			current = advance(current)

		return None

	def crease_key(self):
		'''
		aka increase-/decrease-key
		'''

		raise NotImplementedError

	def delete(self):
		raise NotImplementedError

	def sift(self):
		'''
		Move a node up/down to restore order.
		'''

		raise NotImplementedError


@mutable(slots=False, kw_only=True)
class MinNode(Node):
	@property
	def correct(self):
		return not self.parent or self.parent.key < self.key


@mutable(slots=False, kw_only=True)
class MaxNode(Node):
	@property
	def correct(self):
		return not self.parent or self.parent.key > self.key


@mutable(slots=False, kw_only=True)
class MinHeap(Heap):
	root:MinNode


@mutable(slots=False, kw_only=True)
class MaxHeap(Heap):
	root:MaxNode
