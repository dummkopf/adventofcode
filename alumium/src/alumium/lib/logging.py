'''
Better logging for the usual use-case.

No named levels:
	There is no such thing as INFO or DEBUG messages per se - those are arbitrary.
	Thus, don't even pretend. No need to allow for adding/changing levels, sugar for logging at different levels, etc.

Late binding:
	log(...) can take a callable message, which it calls without args.
	This allows for late-binding, so logging messages don't have to be composed if they're unnecessary.

To Do:
	inspect stack for call location
	metadata/structured data
	configurable routing based on location and/or metadata
'''

required_level = 0


def log(message, level=0, *, args=None, kwargs=None):
	if level >= required_level:
		if callable(message):
			# allows for late-binding
			print(message())
		elif args or kwargs:
			print(message.format(*(args or []), **(kwargs or {})))
		else:
			print(message)
