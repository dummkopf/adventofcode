def subclasses(cls, recursive=False, leaf_only=False):
	subclss = cls.__subclasses__()

	if recursive:
		for subcls in subclss:
			yield from subclasses(subcls, recursive=recursive, leaf_only=leaf_only)

		if leaf_only:
			if not subclss:
				yield cls
		else:
			yield from subclss
	else:
		yield from subclss
