from itertools import chain
from functools import lru_cache
from pathlib import Path
from typing import ClassVar, TextIO, Iterable, Sequence, Mapping, Any
import re

from attrs import mutable, frozen, field, resolve_types

from .logging import log
from .pushback import PushbackBuffer


@frozen(kw_only=True)
class InputFile:
	buffer:PushbackBuffer
	collections:Sequence['Section'] = field(factory=list, init=False)

	SECTIONS:ClassVar[Sequence[type['Section']]]

	@classmethod
	def from_path(cls, path):
		return cls(buffer=PushbackBuffer(Path(path).open('r')))

	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_value, exc_tb):
		self.buffer.close()

	def parse(self):
		for section_cls in self.SECTIONS:
			self.collections.append(section := section_cls(buffer=self.buffer))
			section.parse()


@frozen(kw_only=True, slots=False)
class Section:
	buffer:PushbackBuffer
	records:Sequence['Record'] = field(factory=list, init=False)

	RECORD_CLS:ClassVar[type['Record']]

	def parse(self):
		self.parse_many()

	def parse_many(self):
		while True:
			try:
				(new_record := self.RECORD_CLS(buffer=self.buffer)).parse()
			except StopIteration:
				break

			self.records.append(new_record)

	def parse_one(self):
		(new_record := self.RECORD_CLS(buffer=self.buffer)).parse()
		self.records.append(new_record)


@mutable(kw_only=True, slots=False)
class Record:
	buffer:PushbackBuffer

	def parse(self):
		pass

	def chomp(self, pattern, conv=None) -> Sequence[Any]:
		conv = conv or (lambda x:x)

		line = next(self.buffer)

		if not (match := re.fullmatch(pattern, line)):
			raise StopIteration

		return list(map(conv, match.groups()))

	def chomp_list(self, pattern, conv=None, sep=', ') -> Sequence[Any]:
		raw, = self.chomp(pattern)

		return list(map(conv, raw.split(sep)))

	def chuck_empty(self, all=False):
		while True:
			try:
				line = next(self.buffer)
			except StopIteration:
				break

			if line:
				self.buffer.pushback(line)
				break

			if not all:
				break


resolve_types(InputFile)  #type:ignore
resolve_types(Section)  #type:ignore
