from collections.abc import Iterable, Callable
from typing import Any

from attrs import mutable, field


@mutable(slots=False, kw_only=True)
class Tree:
	root:'Node'


@mutable(slots=False, kw_only=True)
class Node:
	value:Any
	tree:Tree
	parent:Self|None = None
	children:Iterable[Self]

	def bfs(self, cond=Callable[[Self], bool]):
		raise NotImplementedError

	def dfs(self, cond=Callable[[Self], bool]):
		raise NotImplementedError

	@property
	def leaves(self):
		return list(self._leaves())

	def _leaves(self):
		if self.children:
			for child in self.children:
				yield from child._leaves()
		else:
			yield self
