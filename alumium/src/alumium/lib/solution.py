from pathlib import Path
from typing import ClassVar, TypeVar, ForwardRef, Sequence, Any
import inspect
import re
import sys

from .cls import subclasses
from .parse import InputFile, Record
from .package import touch


class Solution:
	YEAR:ClassVar[int]
	DAY:ClassVar[int]
	PART:ClassVar[int]

	INPUT_CLS:ClassVar[TypeVar(InputFile)]

	input_file:InputFile

	def __init__(self, buffer):
		if isinstance(self.INPUT_CLS, ForwardRef):
			subclass_module = sys.modules[self.__class__.__module__]
			self.INPUT_CLS = self.INPUT_CLS._evaluate(vars(subclass_module), {}, set())

		if isinstance(buffer, str):
			buffer = Path(buffer)

		if isinstance(buffer, Path):
			self.input_file = self.INPUT_CLS.from_path(buffer)
		else:
			self.input_file = self.INPUT_CLS(buffer=buffer)

		self.input_file.parse()

	def run(self) -> None:
		pass

	@property
	def answer(self) -> Any:
		pass

	@classmethod
	def find(cls, year, day, part):
		touch('alumium.solutions', recursive=True)

		for subcls in subclasses(cls, recursive=True, leaf_only=True):
			if subcls.YEAR == year and subcls.DAY == day and subcls.PART == part:
				return subcls

	@classmethod
	def with_default_input(cls, suffix='.input'):
		# there's probably a better way to do this
		root_path = Path(__file__).parent.parent.parent.parent

		input_path = root_path / 'inputs' / f'{cls.YEAR:04d}' / f'{cls.DAY:02d}{suffix}'

		return cls(input_path)

	@classmethod
	def with_sample_input(cls):
		return cls.with_default_input('.sample')
