from importlib import import_module
from pkgutil import iter_modules


def touch(package, skip_private=True, recursive=False):
	'''
	Import all child modules of the given package.

	The intent is to ensure the loading of subclasses to enable the use of __subclasses__().
	'''

	###FIXME: recursive option

	if isinstance(package, str):
		package = import_module(package)

	prefix = f'{package.__name__}.'

	if hasattr(package, '__path__'):
		for _, module_name, _ in iter_modules(package.__path__, prefix):
			if not (skip_private and module_name.split('.')[-1].startswith('_')):
				if recursive:
					touch(module_name, skip_private=skip_private, recursive=recursive)
				else:
					import_module(module_name)


def register(target, key=None, container=None):
	container_name = container or getattr(target, '_registry_container', lambda: None)() or '_registry'

	if not hasattr(target, container_name):
		setattr(target, container_name, {})

	container = getattr(target, container_name)

	def inner(obj):
		nonlocal key

		key = key or getattr(obj, '_registry_key', lambda: None)() or (lambda o: o.__name__)

		if callable(key):
			key = key(obj)

		container[key] = obj

		return obj

	return inner
