#!/usr/bin/env python

from argparse import ArgumentParser
from functools import lru_cache
import sys

try:
	import ipdb as pdb
except ImportError:
	import pdb

from ..lib.solution import Solution
from ..lib import logging


###TODO: use pav


class Main(ArgumentParser):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

		self.add_argument('year', type=int)
		self.add_argument('day', type=int)
		self.add_argument('part', type=int)

		self.add_argument('--debug', '--pdb', action='store_true', help='enter pdb on error')

		self.add_argument('--verbose', '-v', action='append_const', const=1, dest='_verbosity')
		self.add_argument('--quiet', '-q', action='append_const', const=-1, dest='_verbosity')

	@property
	@lru_cache
	def args(self):
		return self.parse_args()

	@property
	def verbosity(self):
		return sum(self.args._verbosity) if self.args._verbosity else 0

	def run(self):
		if self.args.debug:
			sys.excepthook = enter_pdb

		logging.required_level = -self.verbosity

		solution_cls = Solution.find(self.args.year, self.args.day, self.args.part)
		solution = solution_cls.with_default_input()

		solution.run()

		print(solution.answer)

	@classmethod
	def main(cls):
		cls().run()


def enter_pdb(exc_type, exc_value, exc_tb):
	sys.__excepthook__(exc_type, exc_value, exc_tb)

	pdb.pm()
