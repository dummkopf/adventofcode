#!/usr/bin/env python

from lib.args import ArgumentParser


'''
'''

DAY_NUMBER = ???


def part1(**__):
    '''
    '''

    return _part1()

def _part1():
    pass

def part2(**__):
    '''
    '''

    return _part2()

def _part2():
    pass

def load(input):
    with input.open() as f:
        pass


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
