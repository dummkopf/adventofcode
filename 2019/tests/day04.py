import pytest

import day04


class part1:
    @pytest.fixture(scope='function', params=[
        (111111, 1),
        (223450, 0),
        (123789, 0)])
    def _actual(self, request):
        startend, expected = request.param
        return day04._part1(startend, startend, None), expected
    
    def examples(self, _actual):
        actual, expected = _actual
        
        assert actual == expected, actual

class part2:
    @pytest.fixture(scope='function', params=[
        (112233, 1),
        (123444, 0),
        (111122, 1)])
    def _actual(self, request):
        startend, expected = request.param
        return day04._part2(startend, startend, None), expected
    
    def examples(self, _actual):
        actual, expected = _actual
        
        assert actual == expected, actual

def two_adjacent_trial():
    assert day04.two_adjacent_trial('111111')
    assert day04.two_adjacent_trial('223450')
    assert not day04.two_adjacent_trial('123789')

def non_descending_trial():
    assert day04.non_descending_trial('111111')
    assert not day04.non_descending_trial('223450')
    assert day04.non_descending_trial('123789')

def strict_double_exists_trial():
    assert day04.strict_double_exists_trial('112233')
    assert not day04.strict_double_exists_trial('123444')
    assert day04.strict_double_exists_trial('111122')
    assert not day04.strict_double_exists_trial('111222')
    assert not day04.strict_double_exists_trial('111111')
    assert not day04.strict_double_exists_trial('111111222222')
    assert not day04.strict_double_exists_trial('111111222222111111')
