import pytest

import day03
from lib.vector import Vector


class WireGrid:
    def init(self):
        sentinel = object()
        grid = day03.WireGrid(sentinel)
        
        assert grid.wires == []
        assert grid.instructionses == (sentinel,)
    
    class from_stream:
        def normal(self, mocker):
            parse_stream_return_value = [[Vector(1,0), Vector(0,-1), Vector(-1,0)]]
            mocker.patch.object(day03.WireGrid, 'parse_stream', return_value=parse_stream_return_value)
            mocker.patch.object(day03.WireGrid, '__init__', return_value=None)
            
            day03.WireGrid.from_stream(['R1,U1,L1'])
            
            day03.WireGrid.__init__.assert_called_once_with(*parse_stream_return_value)
            day03.WireGrid.parse_stream.assert_called_once_with(['R1,U1,L1'])
        
        def functional(self):
            grid = day03.WireGrid.from_stream(['R1,U1,L1','D2,R2,U2'])
            
            assert grid.wires == []
            assert grid.instructionses == (
                [Vector(1,0),Vector(0,-1),Vector(-1,0)],
                [Vector(0,2),Vector(2,0),Vector(0,-2)],
            ), grid.instructionses
    
    class parse_stream:
        @pytest.fixture(scope='function')
        def _mock_grid(self, mocker):
            parse_wire_return_value = [Vector(1,0), Vector(0,-1), Vector(-1,0)]
            mocker.patch.object(day03.WireGrid, 'parse_wire', return_value=parse_wire_return_value)
            
            return day03.WireGrid
        
        def normal(self, _mock_grid):
            list(day03.WireGrid.parse_stream(['R1,U1,L1']))
            
            day03.WireGrid.parse_wire.assert_called_once_with(['R1','U1','L1'])
        
        def blank_lines(self, _mock_grid):
            list(day03.WireGrid.parse_stream(['R1,U1,L1','','  ','\t \t']))
            
            day03.WireGrid.parse_wire.assert_called_once_with(['R1','U1','L1'])
    
    def parse_wire(self):
        pass
    
    def parse_instruction(self):
        assert day03.WireGrid.parse_instruction('R1') == Vector(1,0)
        assert day03.WireGrid.parse_instruction('R12') == Vector(12,0)
        assert day03.WireGrid.parse_instruction('R-12') == Vector(-12,0)
        assert day03.WireGrid.parse_instruction('L1') == Vector(-1,0)
        assert day03.WireGrid.parse_instruction('U1') == Vector(0,-1)
        assert day03.WireGrid.parse_instruction('D1') == Vector(0,1)
        
        with pytest.raises(day03.WireGrid.BadDirection):
            day03.WireGrid.parse_instruction('X1')
    
    class collisions:
        def none(self):
            grid = day03.WireGrid.from_stream(['R1','U1'])
            grid.resolve()
            
            assert grid.collisions == set(), grid.collisions
        
        def one(self):
            grid = day03.WireGrid.from_stream(['R1,U1','U1,R1'])
            grid.resolve()
            
            assert grid.collisions == {Vector(1,-1)}, grid.collisions
        
        def two(self):
            grid = day03.WireGrid.from_stream(['R1,U2','U3,R1,D2'])
            grid.resolve()
            
            assert grid.collisions == {Vector(1,-1),Vector(1,-2)}, grid.collisions
        
        def self(self):
            grid = day03.WireGrid.from_stream(['R3,U1,L1,D2'])
            grid.resolve()
            
            assert grid.collisions == set(), grid.collisions
        
        def atzero(self):
            grid = day03.WireGrid.from_stream(['R1,L1','U1,D1'])
            grid.resolve()
            
            assert grid.collisions == {Vector(0,0)}, grid.collisions
    
    class time_to_collisions:
        def simple(self):
            wire = [4,2,0,6,8,10]
            collisions = [2, 8]
            
            actual = day03.WireGrid.time_to_collisions(wire, collisions)
            
            assert actual == {2: 2, 8: 5}
    
    class timed_collisions:
        def simple(self):
            grid = day03.WireGrid.from_stream(['R1,U1','U1,R1']).resolve()
            
            assert grid.timed_collisions == {Vector(1,-1): [2,2]}, grid.timed_collisions
    
    class resolve:
        def basic(self, mocker):
            mocker.patch.object(day03.WireGrid, 'resolve_wire', return_value=[])
            
            grid = day03.WireGrid.from_stream(['R1,U1','U1,R1'])
            grid.resolve()
            
            day03.WireGrid.resolve_wire.assert_any_call([Vector(1,0), Vector(0,-1)])
            day03.WireGrid.resolve_wire.assert_any_call([Vector(0,-1), Vector(1, 0)])
    
    class resolve_wire:
        pass
    
    class resolve_line:
        def point(self):
            assert list(day03.WireGrid.resolve_line(Vector(0,0),Vector(0,0))) == []
            assert list(day03.WireGrid.resolve_line(Vector(1,-1),Vector(1,-1))) == []
        
        def two_long(self):
            points = list(day03.WireGrid.resolve_line(Vector(0,0),Vector(1,0)))
            assert points == [Vector(1,0)], points
            points = list(day03.WireGrid.resolve_line(Vector(0,0),Vector(-1,0)))
            assert points == [Vector(-1,0)], points
            points = list(day03.WireGrid.resolve_line(Vector(0,0),Vector(0,1)))
            assert points == [Vector(0,1)], points
            points = list(day03.WireGrid.resolve_line(Vector(0,0),Vector(0,-1)))
            assert points == [Vector(0,-1)], points
            points = list(day03.WireGrid.resolve_line(Vector(4,5),Vector(4,6)))
            assert points == [Vector(4,6)], points
        
        def ten_long(self):
            points = list(day03.WireGrid.resolve_line(Vector(0,0),Vector(10,0)))
            assert points == [Vector(x,0) for x in range(1,11)], points
            points = list(day03.WireGrid.resolve_line(Vector(0,0),Vector(-10,0)))
            assert points == [Vector(x,0) for x in range(-1,-11,-1)], points
            points = list(day03.WireGrid.resolve_line(Vector(0,0),Vector(0,10)))
            assert points == [Vector(0,x) for x in range(1,11)], points
            points = list(day03.WireGrid.resolve_line(Vector(0,0),Vector(0,-10)))
            assert points == [Vector(0,x) for x in range(-1,-11,-1)], points
            points = list(day03.WireGrid.resolve_line(Vector(4,5),Vector(4,15)))
            assert points == [Vector(4,x) for x in range(6,16)], points

class part1:
    @pytest.fixture(scope='function', params=[
        (['R8,U5,L5,D3', 'U7,R6,D4,L4'], 6),
        (['R75,D30,R83,U83,L12,D49,R71,U7,L72', 'U62,R66,U55,R34,D71,R55,D58,R83'], 159),
        (['R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51', 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'], 135),
    ])
    def _wiregrid(self, request):
        instructionses,expected = request.param
        
        return day03.WireGrid.from_stream(instructionses), expected
    
    def normal(self, _wiregrid):
        grid, expected = _wiregrid
        
        location, actual = day03._part1(grid, None)
        
        assert actual == expected, f'{actual} @ {location} != {expected}'

class part2:
    @pytest.fixture(scope='function', params=[
        (['R8,U5,L5,D3', 'U7,R6,D4,L4'], 30),
        (['R75,D30,R83,U83,L12,D49,R71,U7,L72', 'U62,R66,U55,R34,D71,R55,D58,R83'], 610),
        (['R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51', 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'], 410),
    ])
    def _wiregrid(self, request):
        instructionses,expected = request.param
        
        return day03.WireGrid.from_stream(instructionses), expected
    
    def normal(self, _wiregrid):
        grid, expected = _wiregrid
        
        actual = day03._part2(grid, None)
        
        assert actual == expected, f'{actual} != {expected}'
