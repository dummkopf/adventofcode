import pytest

import day06


class part1:
    def example(self):
        orbits = day06.OrbitCollection.from_text('''
            COM)B
            B)C
            C)D
            D)E
            E)F
            B)G
            G)H
            D)I
            E)J
            J)K
            K)L
        ''')
        
        assert day06._part1(orbits) == 42

class part2:
    def example(self, mocker):
        orbits = day06.OrbitCollection.from_text('''
            COM)B
            B)C
            C)D
            D)E
            E)F
            B)G
            G)H
            D)I
            E)J
            J)K
            K)L
            K)YOU
            I)SAN
        ''')
        
        actual = day06._part2(orbits, mocker.MagicMock())
        
        assert actual == 4, actual

class OrbitCollection:
    class init:
        def empty(self):
            fixture = day06.OrbitCollection([])
            
            assert fixture.orbits == {}
        
        def single(self):
            orbit = day06.Orbit('A', 'B')
            
            fixture = day06.OrbitCollection([orbit])
            
            assert fixture.orbits == {'A': orbit}, fixture.orbits
    
    class from_text:
        def empty(self):
            collection = day06.OrbitCollection.from_text('')
            
            assert collection.orbits == {}
        
        def single(self):
            collection = day06.OrbitCollection.from_text('A)B')
            
            assert len(collection.orbits) == 2
            assert collection.orbits['A']._parent is None
            assert collection.orbits['B']._parent == 'A'
        
        def blanklines(self):
            collection = day06.OrbitCollection.from_text('  \nA)B\nB)C\n')
            
            assert len(collection.orbits) == 3
            assert collection.orbits['A']._parent is None
            assert collection.orbits['B']._parent == 'A'
            assert collection.orbits['C']._parent == 'B'
        
        def trimlines(self):
            collection = day06.OrbitCollection.from_text('  \n A)B \nB)C\n')
            
            assert len(collection.orbits) == 3
            assert collection.orbits['A']._parent is None
            assert collection.orbits['B']._parent == 'A'
            assert collection.orbits['C']._parent == 'B'

class Orbit:
    @pytest.mark.skip
    def init(self):
        pass
    
    class parent:
        @pytest.mark.skip
        def string(self):
            pass
        
        @pytest.mark.skip
        def orbit(self):
            pass
    
    class ancestors:
        def empty(self):
            collection = day06.OrbitCollection.from_text('A)B')
            
            assert collection['A'].ancestors == []
        
        def single(self):
            collection = day06.OrbitCollection.from_text('A)B')
            
            actual = collection['B'].ancestors
            
            assert [x.name for x in actual] == ['A']
        
        def many(self):
            collection = day06.OrbitCollection.from_text('A)B\nB)C\nC)D\nD)E')
            
            actual = collection['E'].ancestors
            
            assert [x.name for x in actual] == ['D','C','B','A']
        
        def many_with_branch(self):
            collection = day06.OrbitCollection.from_text('A)B\nB)C\nC)D\nD)E\nC)F\nF)G\nG)H')
            
            actual = collection['E'].ancestors
            
            assert [x.name for x in actual] == ['D','C','B','A']
    
    class common_ancestor:
        def same(self):
            collection = day06.OrbitCollection.from_text('A)B')
            
            A = collection['A']
            B = collection['B']
            
            assert B.common_ancestor(B) == A
        
        def direct(self):
            collection = day06.OrbitCollection.from_text('A)B\nB)C')
            
            A = collection['A']
            B = collection['B']
            C = collection['C']
            
            assert B.common_ancestor(C) == A
            assert C.common_ancestor(B) == A
        
        def linear(self):
            collection = day06.OrbitCollection.from_text('A)B\nB)C\nC)D')
            
            B = collection['B']
            C = collection['C']
            D = collection['D']
            
            assert C.common_ancestor(D) == B
            assert D.common_ancestor(C) == B
        
        def cousin(self):
            collection = day06.OrbitCollection.from_text('X)A\nA)B\nA)C\nC)D')
            
            A = collection['A']
            B = collection['B']
            D = collection['D']
            
            assert D.common_ancestor(B) == A
            assert B.common_ancestor(D) == A
        
        def root(self):
            collection = day06.OrbitCollection.from_text('A)B\nA)C\nC)D')
            
            A = collection['A']
            D = collection['D']
            
            with pytest.raises(day06.Orbit.errors.NotFound):
                D.common_ancestor(A) == A
            
            with pytest.raises(day06.Orbit.errors.NotFound):
                A.common_ancestor(D) == A
    
    class distance_to:
        def same(self):
            collection = day06.OrbitCollection.from_text('A)B')
            
            assert collection['A'].distance_to(collection['A']) == 0
        
        def not_found(self):
            collection = day06.OrbitCollection.from_text('A)B\nA)C')
            
            with pytest.raises(day06.Orbit.errors.NotFound):
                collection['B'].distance_to(collection['C'])
        
        def adjacent(self):
            collection = day06.OrbitCollection.from_text('A)B')
            
            assert collection['B'].distance_to(collection['A']) == 1
        
        def distant(self):
            collection = day06.OrbitCollection.from_text('A)B\nB)C\nA)D\nD)E\nE)F\nE)G\nG)H')
            
            assert collection['H'].distance_to(collection['D']) == 3
        
        def root(self):
            collection = day06.OrbitCollection.from_text('A)B\nB)C\nA)D\nD)E\nE)F\nE)G\nG)H')
            
            assert collection['H'].distance_to(collection['A']) == 4
