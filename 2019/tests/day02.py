import pytest

import day02


class Part1:
    def example0(self):
        codes = 1,9,10,3,2,3,11,0,99,30,40,50
        expected = [3500,9,10,70,2,3,11,0,99,30,40,50]
        
        fixture = day02.IntcodeEngine(codes)
        list(fixture.run())
        
        actual = fixture.memory
        
        assert actual == expected, actual
    
    def example1(self):
        codes = 1,0,0,0,99
        expected = [2,0,0,0,99]
        fixture = day02.IntcodeEngine(codes)
        list(fixture.run())
        
        actual = fixture.memory
        
        assert actual == expected, actual
    
    def example2(self):
        codes = 2,3,0,3,99
        expected = [2,3,0,6,99]
        fixture = day02.IntcodeEngine(codes)
        list(fixture.run())
        
        actual = fixture.memory
        
        assert actual == expected, actual
    
    def example3(self):
        codes = 2,4,4,5,99,0
        expected = [2,4,4,5,99,9801]
        fixture = day02.IntcodeEngine(codes)
        list(fixture.run())
        
        actual = fixture.memory
        
        assert actual == expected, actual
    
    def example4(self):
        codes = 1,1,1,4,99,5,6,0,99
        expected = [30,1,1,4,2,5,6,0,99]
        fixture = day02.IntcodeEngine(codes)
        list(fixture.run())
        
        actual = fixture.memory
        
        assert actual == expected, actual

class Part2:
    pass
