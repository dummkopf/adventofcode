from collections.abc import Generator

import pytest

from lib import intcode


class IntcodeEngine:
    class init:
        def program_counter(self):
            engine = intcode.IntcodeEngine([])
            
            assert engine.program_counter == 0
        
        def codes(self):
            assert intcode.IntcodeEngine([]).memory == []
            assert intcode.IntcodeEngine(tuple()).memory == []
            assert intcode.IntcodeEngine(set()).memory == []
            assert intcode.IntcodeEngine(range(0)).memory == []
            
            assert intcode.IntcodeEngine([1,2,3]).memory == [1,2,3]
            assert intcode.IntcodeEngine((1,2,3)).memory == [1,2,3]
            assert intcode.IntcodeEngine({1,2,3}).memory == [1,2,3]
            assert intcode.IntcodeEngine(range(1,4)).memory == [1,2,3]
        
    def getitem(self, mocker):
        fixture = mocker.MagicMock()
        
        intcode.IntcodeEngine.__getitem__(fixture, 4)
        
        fixture.memory.__getitem__.assert_called_once_with(4)
    
    def setitem(self, mocker):
        fixture = mocker.MagicMock()
        
        intcode.IntcodeEngine.__setitem__(fixture, 4, 17)
        
        fixture.memory.__setitem__.assert_called_once_with(4, 17)
    
    @pytest.mark.skip
    class iter:
        def fresh(self):
            pass
        
        def stale(self):
            pass
        
    class copy:
        def basic(self):
            original = intcode.IntcodeEngine([1,2,3])
            copy = original.copy()
            
            assert copy.memory == [1,2,3]
            assert copy.program_counter == 0
        
        def from_changed(self):
            original = intcode.IntcodeEngine([1,2,3])
            original[1] = 4
            
            copy = original.copy()
            
            assert copy.memory == [1,4,3]
            assert copy.program_counter == 0

    class run:
        def basic(self, mocker):
            mocker.patch.object(intcode.Instruction, 'from_memory')
            fixture = mocker.Mock()
            fixture.configure_mock(**{'run_current.side_effect': intcode.Instruction.signals.End})
            
            list(intcode.IntcodeEngine.run(fixture))
            fixture.run_current.assert_called_once()
        
        @pytest.mark.skip
        def coro(self, mocker):
            pass
    
    class run_current:
        def basic(self, mocker):
            fixture = mocker.MagicMock(**{'program_counter': 4})
            instruction = mocker.Mock(**{'nargs': 2})
            
            intcode.IntcodeEngine.run_current(fixture, instruction)
            
            instruction.assert_called_once_with()
            assert fixture.program_counter == 7
        
        @pytest.mark.skip
        def coro(self, mocker):
            pass
        
        def jump(self):
            fixture = intcode.IntcodeEngine([])
            
            assert fixture.program_counter == 0
            
            def fake_instruction():
                raise intcode.Instruction.signals.Jump(4)
            
            fixture.run_current(fake_instruction)
            
            assert fixture.program_counter == 4


class Instruction:
    @pytest.mark.skip
    def init(self):
        pass
    
    @pytest.mark.skip
    def getitem(self):
        pass
    
    @pytest.mark.skip
    def setitem(self):
        pass
    
    def call(self):
        with pytest.raises(NotImplementedError):
            intcode.Instruction([])()
    
    class next:
        def empty(self):
            fixture = intcode.Instruction([], iter([]))
            
            with pytest.raises(StopIteration):
                fixture.next
        
        def single(self):
            sentinel = object()
            fixture = intcode.Instruction([], iter([sentinel]))
            
            assert fixture.next is sentinel
        
        def none(self):
            fixture = intcode.Instruction([], None)
            
            with pytest.raises(TypeError, match=r'object is not an iterator$'):
                fixture.next
    
    def from_memory(self, mocker):
        fakesub = mocker.Mock(**{'nargs': 3})
        mocker.patch.object(intcode.Instruction, 'getsub', return_value=fakesub)
        mocker.patch.object(intcode.Param, 'parse', return_value=object())
        
        memory = [1003, 1, 2, 3]
        
        intcode.Instruction.from_memory(memory, 0, None)
        
        intcode.Instruction.getsub.assert_called_once_with(3)
        fakesub.assert_called_once_with(intcode.Param.parse.return_value, None)
    
    @pytest.mark.skip
    def getsub(self):
        pass

class AddInstruction:
    def call(self, mocker):
        fixture = mocker.MagicMock(**{'__getitem__.return_value': 4})
        
        intcode.AddInstruction.__call__(fixture)
        
        fixture.__setitem__.assert_called_once_with(2, 8)

class MultiplyInstruction:
    def call(self, mocker):
        fixture = mocker.MagicMock(**{'__getitem__.return_value': 4})
        
        intcode.MultiplyInstruction.__call__(fixture)
        
        fixture.__setitem__.assert_called_once_with(2, 16)

class InputInstruction:
    def call(self, mocker):
        mocker.patch.object(intcode.InputInstruction, '__setitem__')
        fixture = intcode.InputInstruction([], iter([4]))
        
        fixture()
        
        fixture.__setitem__.assert_called_once_with(0, 4)

class OutputInstruction:
    def call(self, mocker):
        mocker.patch.object(intcode.OutputInstruction, '__getitem__', return_value=4)
        fixture = intcode.OutputInstruction([])
        
        result = fixture()
        
        assert result == 4
        fixture.__getitem__.assert_called_once_with(0)

class JumpIfTrueInstruction:
    def call(self, mocker):
        mocker.patch.object(intcode.JumpIfTrueInstruction, '__getitem__', lambda s,k: [True,4][k])
        mocker.patch.object(intcode.JumpIfTrueInstruction, '__setitem__')
        
        with pytest.raises(intcode.Instruction.signals.Jump) as ei:
            intcode.JumpIfTrueInstruction([])()
        
        assert ei.value.position == 4
    
    def call_bad(self, mocker):
        mocker.patch.object(intcode.JumpIfTrueInstruction, '__getitem__', lambda s,k: [False,4][k])
        mocker.patch.object(intcode.JumpIfTrueInstruction, '__setitem__')
        
        intcode.JumpIfTrueInstruction([])()

class JumpIfFalseInstruction:
    def call(self, mocker):
        mocker.patch.object(intcode.JumpIfFalseInstruction, '__getitem__', lambda s,k: [False,4][k])
        
        with pytest.raises(intcode.Instruction.signals.Jump) as ei:
            intcode.JumpIfFalseInstruction([])()
        
        assert ei.value.position == 4
    
    def call(self, mocker):
        mocker.patch.object(intcode.JumpIfFalseInstruction, '__getitem__', lambda s,k: [True,4][k])
        
        intcode.JumpIfFalseInstruction([])()

class LessThanInstruction:
    @pytest.fixture(scope='function', params=[
        (2, 4, 1),
        (8, 4, 0),
        (4, 4, 0),
    ])
    def _mocked(self, request, mocker):
        arg0, arg1, expected = request.param
        
        mocker.patch.object(
            intcode.LessThanInstruction,
            '__getitem__',
            lambda s,k: [arg0, arg1][k]
        )
        mocker.patch.object(intcode.LessThanInstruction, '__setitem__')
        
        fixture = intcode.LessThanInstruction([])
        
        return fixture, expected
    
    def call(self, _mocked):
        fixture, expected = _mocked
        
        fixture()
        
        fixture.__setitem__.assert_called_once_with(2, expected)

class EqualsInstruction:
    @pytest.fixture(scope='function', params=[
        (2, 4, 0),
        (8, 4, 0),
        (4, 4, 1),
    ])
    def _mocked(self, request, mocker):
        arg0, arg1, expected = request.param
        
        mocker.patch.object(
            intcode.EqualsInstruction,
            '__getitem__',
            lambda s,k: [arg0, arg1][k]
        )
        mocker.patch.object(intcode.EqualsInstruction, '__setitem__')
        
        fixture = intcode.EqualsInstruction([])
        
        return fixture, expected
    
    def call(self, _mocked):
        fixture, expected = _mocked
        
        fixture()
        
        fixture.__setitem__.assert_called_once_with(2, expected)

class EndInstruction:
    def call(self, mocker):
        with pytest.raises(intcode.Instruction.signals.End):
            intcode.EndInstruction([])()


class Param:
    @pytest.mark.skip
    def init(self):
        pass
    
    class call:
        @pytest.mark.skip
        def get(self):
            pass
        
        @pytest.mark.skip
        def set(self):
            pass
        
        @pytest.mark.skip
        def bad(self):
            pass
    
    def parse(self, mocker):
        mocker.patch.object(intcode.Param, 'factory')
        memory = object()
        
        iteractual = intcode.Param.parse(12303, [1, 2, 3, 4], memory)
        
        next(iteractual)
        intcode.Param.factory.assert_called_with(3, 1, memory)
        next(iteractual)
        intcode.Param.factory.assert_called_with(2, 2, memory)
        next(iteractual)
        intcode.Param.factory.assert_called_with(1, 3, memory)
        next(iteractual)
        intcode.Param.factory.assert_called_with(0, 4, memory)
        
        with pytest.raises(StopIteration):
            next(iteractual)
        assert intcode.Param.factory.call_count == 4
    
    @pytest.mark.skip
    class factory:
        pass

class LitParam:
    @pytest.mark.skip
    def get(self):
        pass
    
    @pytest.mark.skip
    def set(self):
        pass

class RefParam:
    @pytest.mark.skip
    def init(self):
        pass
    
    @pytest.mark.skip
    def get(self):
        pass
    
    @pytest.mark.skip
    def set(self):
        pass
