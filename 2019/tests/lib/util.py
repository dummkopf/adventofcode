from lib import util


def reverse_digits():
    assert util.reverse_digits(123) == 321
    assert util.reverse_digits(-123) == -321
    assert util.reverse_digits(0) == 0
    assert util.reverse_digits(1) == 1
    assert util.reverse_digits(10) == 1

def sign():
    assert util.sign(1) == 1
    assert util.sign(0) == 1
    assert util.sign(-1) == -1
    assert util.sign(-10) == -1
    assert util.sign(10) == 1
    assert util.sign(104583128349) == 1
