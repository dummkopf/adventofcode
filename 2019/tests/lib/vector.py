import pytest

from lib.vector import Vector


class init:
    def splat(self):
        assert Vector(1).axes == (1,)
        assert Vector(1,2).axes == (1,2)
        assert Vector(1,2,3).axes == (1,2,3)
    
    def single(self):
        assert Vector([1,2,3]).axes == (1,2,3)
        assert Vector((1,2,3)).axes == (1,2,3)
        assert Vector(range(3)).axes == (0,1,2)
        assert Vector(x for x in range(3)).axes == (0,1,2)

def repr_():
    assert repr(Vector(1,2,3)) == '<Vector ((1, 2, 3))>'

def str_():
    assert str(Vector(1,2,3)) == '<Vector ((1, 2, 3))>'

class eq:
    def vector(self):
        assert Vector(1,2,3) == Vector(1,2,3)
    
    def tuple_(self):
        assert Vector(1,2,3) == (1,2,3)

class lt:
    def vector(self):
        assert Vector(1,2,3) < Vector(1,2,4)
    
    def tuple_(self):
        assert Vector(1,2,3) < (1,2,4)

class gt:
    def vector(self):
        assert Vector(1,2,4) > Vector(1,2,3)
    
    def tuple_(self):
        assert Vector(1,2,4) > (1,2,3)

class le:
    def vector(self):
        assert Vector(1,2,3) <= Vector(1,2,4)
        assert Vector(1,2,3) <= Vector(1,2,3)
    
    def tuple_(self):
        assert Vector(1,2,3) <= (1,2,4)
        assert Vector(1,2,3) <= (1,2,3)

class ge:
    def vector(self):
        assert Vector(1,2,4) >= Vector(1,2,3)
        assert Vector(1,2,4) >= Vector(1,2,4)
    
    def tuple_(self):
        assert Vector(1,2,4) >= (1,2,3)
        assert Vector(1,2,4) >= (1,2,4)

class ne:
    def vector(self):
        assert Vector(1,2,3) != Vector(1,2)
    
    def tuple_(self):
        assert Vector(1,2,3) != (1,2)

class add:
    def normal(self):
        assert Vector(1,2,3) + Vector(3,2,1) == Vector(4,4,4)
        assert Vector(1,2,3) + Vector(-1,-2,-3) == Vector(0,0,0)
        assert Vector(1,2,3) + Vector(0,0,0) == Vector(1,2,3)
        assert Vector(1,2,3) + Vector(-4,-4,-4) == Vector(-3,-2,-1)
    
    def other_iter(self):
        assert Vector(1,2,3) + (3,2,1) == Vector(4,4,4)
        assert Vector(1,2,3) + [3,2,1] == Vector(4,4,4)
        assert Vector(1,2,3) + (x for x in (3,2,1)) == Vector(4,4,4)
    
    def mismatch(self):
        with pytest.raises(Vector.CardinalityMismatch):
            Vector(1,2,3) + Vector(1)
        with pytest.raises(Vector.CardinalityMismatch):
            Vector(1,2,3) + (1,)

class radd:
    def other_iter(self):
        assert Vector(1,2,3) + (3,2,1) == (4,4,4)
        assert Vector(1,2,3) + [3,2,1] == [4,4,4]
        assert Vector(1,2,3) + (x for x in (3,2,1)) == Vector(4,4,4)
    
    def mismatch(self):
        with pytest.raises(Vector.CardinalityMismatch):
            (1,2,3) + Vector(1)
        with pytest.raises(Vector.CardinalityMismatch):
            (1,) + Vector(1,2,3)

class rmul:
    def normal(self):
        assert -1 * Vector(1,2,3) == (-1,-2,-3)
        assert 0 * Vector(1,2,3) == (0,0,0)
        assert 1 * Vector(1,2,3) == (1,2,3)
        assert 0.5 * Vector(2,4,6) == (1,2,3)

class sub:
    def normal(self):
        assert Vector(1,2,3) - Vector(3,2,1) == Vector(-2,0,2)
        assert Vector(1,2,3) - Vector(-1,-2,-3) == Vector(2,4,6)
        assert Vector(1,2,3) - Vector(0,0,0) == Vector(1,2,3)
        assert Vector(1,2,3) - Vector(4,4,4) == Vector(-3,-2,-1)
    
    def other_iter(self):
        assert Vector(1,2,3) - (3,2,1) == Vector(-2,0,2)
        assert Vector(1,2,3) - [3,2,1] == Vector(-2,0,2)
        assert Vector(1,2,3) - iter((3,2,1)) == Vector(-2,0,2)
    
    def mismatch(self):
        with pytest.raises(Vector.CardinalityMismatch):
            Vector(1,2,3) - Vector(1)
        with pytest.raises(Vector.CardinalityMismatch):
            Vector(1,2,3) - (1,)

class normalized:
    def normal(self):
        assert Vector(0,0).normalized == (0,0)
        assert Vector(1,0).normalized == (1,0)
        assert Vector(5,0).normalized == (1,0)
        assert Vector(5,0,0).normalized == (1,0,0)
        assert Vector(-5,0,0).normalized == (-1,0,0)
        assert Vector(-5,5,0).normalized == (-1,1,0)

class magnitude:
    pass

class rect_magnitude:
    def normal(self):
        assert Vector(1,0).rect_magnitude == 1
        assert Vector(1,1).rect_magnitude == 1
        assert Vector(2,1).rect_magnitude == 2
        assert Vector(-2,1).rect_magnitude == 2
        assert Vector(-2,1,-1).rect_magnitude == 2

class manhattan_magnitude:
    def normal(self):
        assert Vector(1,0).manhattan_magnitude == 1
        assert Vector(1,1).manhattan_magnitude == 2
        assert Vector(2,1).manhattan_magnitude == 3
        assert Vector(-2,1).manhattan_magnitude == 3
        assert Vector(-2,1,-1).manhattan_magnitude == 4

class cardinality:
    def normal(self):
        assert Vector(1).cardinality == 1
        assert Vector(1,1).cardinality == 2
        assert Vector(1,2).cardinality == 2
        assert Vector(1,2,3).cardinality == 3

class dot_product:
    pass

class angle_between:
    pass

class copy:
    def normal(self):
        basis = Vector(1,0)
        copy = basis.copy()
        
        assert copy == (1,0)
        assert copy == basis
        assert copy is not basis

class manhattan_distance:
    pass
