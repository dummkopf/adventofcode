import day01


class Part1:
    def given_examples(self):
        assert day01.fuel_requirement(12) == 2
        assert day01.fuel_requirement(14) == 2
        assert day01.fuel_requirement(1969) == 654
        assert day01.fuel_requirement(100756) == 33583


class Part2:
    def given_examples(self, mocker):
        mock_logger = mocker.Mock()
        
        assert day01.recursive_fuel_requirement(14, mock_logger) == 2
        assert day01.recursive_fuel_requirement(1969, mock_logger) == 966
        assert day01.recursive_fuel_requirement(100756, mock_logger) == 50346
