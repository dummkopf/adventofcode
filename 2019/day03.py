#!/usr/bin/env python

from collections import defaultdict
from functools import reduce
from itertools import product, combinations

from lib.args import ArgumentParser
from lib.vector import Vector


'''
Opening the front panel reveals a jumble of wires. Specifically, two wires are
connected to a central port and extend outward on a grid. You trace the path
each wire takes as it leaves the central port, one wire per line of text (your
puzzle input).

The wires twist and turn, but the two wires occasionally cross paths. To fix
the circuit, you need to find the intersection point closest to the central
port. Because the wires are on a grid, use the Manhattan distance for this
measurement. While the wires do technically cross right at the central port
where they both start, this point does not count, nor does a wire count as
crossing with itself.
'''


def part1(input=None, logger=None, **__):
    '''
    What is the Manhattan distance from the central port to the closest
    intersection?
    '''
    
    with input.open('r') as f:
        return _part1(WireGrid.from_stream(f), logger)

def _part1(grid, logger):
    closest = min(grid.resolve().collisions, key=lambda x: x.manhattan_magnitude)
    
    return closest, closest.manhattan_magnitude

def part2(input=None, logger=None, **__):
    '''
    It turns out that this circuit is very timing-sensitive; you actually need
    to minimize the signal delay.

    To do this, calculate the number of steps each wire takes to reach each
    intersection; choose the intersection where the sum of both wires' steps
    is lowest. If a wire visits a position on the grid multiple times, use the
    steps value from the first time it visits that position when calculating
    the total value of a specific intersection.

    The number of steps a wire takes is the total number of grid squares the
    wire has entered to get to that location, including the intersection being
    considered.
    
    What is the fewest combined steps the wires must take to reach an
    intersection?
    '''
    
    with input.open('r') as f:
        return _part2(WireGrid.from_stream(f), logger)

def _part2(grid, logger):
    location, shortest = min(((p,sum(t)) for p,t in grid.resolve().timed_collisions.items()), key=lambda x: x[1])
    
    return shortest


class WireGrid:
    class BadDirection(Exception): pass
    
    def __init__(self, *instructionses):
        self.wires = []
        self.instructionses = instructionses
    
    @classmethod
    def from_stream(cls, stream):
        return cls(*cls.parse_stream(stream))
    
    @classmethod
    def parse_stream(cls, stream):
        for line in stream:
            if line.strip():
                yield cls.parse_wire(line.strip().split(','))
    
    @classmethod
    def parse_wire(cls, instructions):
        return [cls.parse_instruction(x) for x in instructions]
    
    @classmethod
    def parse_instruction(cls, instruction_str):
        if instruction_str[0] == 'R':
            direction = Vector(1, 0)
        elif instruction_str[0] == 'L':
            direction = Vector(-1, 0)
        elif instruction_str[0] == 'U':
            direction = Vector(0, -1)
        elif instruction_str[0] == 'D':
            direction = Vector(0, 1)
        else:
            raise cls.BadDirection
        
        distance = int(instruction_str[1:])
        
        return distance * direction
    
    @property
    def collisions(self):
        deduped_wires = [set(x) for x in self.wires]
        return reduce(set.union, (x & y for x,y in combinations(deduped_wires, 2)), set())
    
    @classmethod
    def time_to_collisions(cls, wire, collisions):
        return {x: wire.index(x) + 1 for x in collisions}
    
    @property
    def timed_collisions(self):
        collisions = self.collisions
        
        merged_collisions = defaultdict(list)
        
        for wire in self.wires:
            for p,t in self.time_to_collisions(wire, collisions).items():
                merged_collisions[p].append(t)
        
        return merged_collisions
    
    def resolve(self):
        self.wires = [list(self.resolve_wire(x)) for x in self.instructionses]
        
        return self
    
    @classmethod
    def resolve_wire(cls, instructions):
        current_position = Vector(0,0)
        # yield current_position #assume origin
        
        for instruction in instructions:
            next_position = current_position + instruction
            
            yield from cls.resolve_line(current_position, next_position)
            
            current_position = next_position
    
    @classmethod
    def resolve_line(cls, start, end):
        vector = end - start
        nvector = vector.normalized
        
        for i in range(1, vector.rect_magnitude + 1):
            position = start + i * nvector
            yield position


if __name__=='__main__':
    ArgumentParser(3, part1, part2).do_main()
