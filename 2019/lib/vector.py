from collections.abc import Iterable
from itertools import product
from numbers import Number
import math


###TODO: optionally enforced domain (int, float, etc)


class Vector:
    class CardinalityMismatch(Exception): pass
    
    def __init__(self, *p):
        if len(p) == 1 and isinstance(p[0], Iterable):
            self.axes = tuple(p[0])
        else:
            self.axes = tuple(p)
    
    def __repr__(self):
        return f'<Vector ({self.axes!r})>'
    __str__=__repr__
    
    def __eq__(self, other):
        return self.axes == tuple(other)
    
    def __lt__(self, other):
        return self.axes < tuple(other)
    
    def __gt__(self, other):
        return self.axes > tuple(other)
    
    def __le__(self, other):
        return self == other or self < other
    
    def __ge__(self, other):
        return self == other or self > other
    
    def __ne__(self, other):
        return not self == other
    
    def __hash__(self):
        return hash(self.__class__) ^ hash(self.axes)
    
    def __add__(self, other):
        if not isinstance(other, Vector):
            other = Vector(other)
        
        self._must_match_cardinality(other)
        
        return self.__class__(sum(x) for x in zip(self, other))
    
    def __radd__(self, other):
        if not isinstance(other, Vector):
            other = Vector(other)
        
        self._must_match_cardinality(other)
        
        return other.__class__(sum(x) for x in zip(self, other))
    
    def __rmul__(self, other):
        if isinstance(other, Number):
            return self.__class__(x*y for x,y in product([other], self))
        else:
            return NotImplemented
    
    def __sub__(self, other):
        if isinstance(other, Vector):
            return self + -1 * other
        else:
            return self + -1 * Vector(other)
    
    def __iter__(self):
        yield from self.axes
    
    def __len__(self):
        return self.cardinality
    
    @property
    def normalized(self):
        divisor = max(map(abs, self)) or 1
        
        return self.__class__(x/divisor for x in self)
    
    @property
    def magnitude(self):
        return sum(x**2 for x in self) ** (1/self.cardinality)
    
    @property
    def rect_magnitude(self):
        return max(map(abs, self))
    
    @property
    def manhattan_magnitude(self):
        return sum(map(abs, self))
    
    @property
    def cardinality(self):
        return len(self.axes)
    
    def dot_product(self, other):
        self._must_match_cardinality(other)
        
        return sum(x*y for x,y in zip(self, other))
    
    def angle_between(self, other):
        return math.acos(self.dot_product(other) / self.magnitude / other.magnitude)
    
    def copy(self):
        return self.__class__(*self)
    
    def manhattan_distance(self, other):
        self._must_match_cardinality(other)
        
        return (self - other).manhattan_magnitude
    
    def _must_match_cardinality(self, other):
        if not len(self) == len(other):
            raise self.CardinalityMismatch
