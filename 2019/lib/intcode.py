class IntcodeEngine:
    def __init__(self, memory):
        self.program_counter = 0
        
        self.memory = list(memory)
    
    def __getitem__(self, key):
        return self.memory[key]
    
    def __setitem__(self, key, value):
        self.memory[key] = value
    
    def copy(self):
        return self.__class__(self.memory)
    
    def run(self, input=None):
        while True:
            try:
                result = self.run_current(Instruction.from_memory(self.memory, self.program_counter, input))
            except Instruction.signals.End:
                break
            
            if result is not None:
                yield result
    
    def run_current(self, instruction):
        try:
            result = instruction()
        except Instruction.signals.Jump as e:
            self.program_counter = e.position
        else:
            self.program_counter += instruction.nargs + 1
            
            return result


class Instruction:
    opcode = None
    _submap = None
    
    class signals:
        class End(Exception): pass
        
        class Jump(Exception):
            def __init__(self, position):
                self.position = position
    
    def __init__(self, args, input=None):
        self.args = list(args)
        self.input = input
    
    def __getitem__(self, key):
        return self.args[key]()
    
    def __setitem__(self, key, value):
        self.args[key](value)
    
    def __call__(self):
        raise NotImplementedError
    
    @property
    def next(self):
        if self.input:
            ###NOTE: if already an iterator, calling iter() should do nothing
            self.input = iter(self.input)
        
        return next(self.input)
    
    @classmethod
    def from_memory(cls, memory, pc, input):
        stream = iter(memory[pc:])
        
        combicode = next(stream)
        opcode = combicode % 100
        
        sub = cls.getsub(opcode)
        
        args = [x for x,y in zip(stream, range(sub.nargs))]
        
        return sub(Param.parse(combicode, args, memory), input)
    
    @classmethod
    def getsub(cls, opcode):
        if cls._submap is None:
            cls._generate_submap()
        
        return cls._submap[opcode]
    
    @classmethod
    def _generate_submap(cls):
        cls._submap = {x.opcode: x for x in cls.__subclasses__()}

class AddInstruction(Instruction):
    opcode = 1
    nargs = 3
    
    def __call__(self):
        self[2] = self[0] + self[1]

class MultiplyInstruction(Instruction):
    opcode = 2
    nargs = 3
    
    def __call__(self):
        self[2] = self[0] * self[1]

class InputInstruction(Instruction):
    '''
    Opcode 3 takes a single integer as input and saves it to the position given by its
    only parameter. For example, the instruction 3,50 would take an input value and store
    it at address 50.
    '''
    
    opcode = 3
    nargs = 1
    
    def __call__(self):
        self[0] = self.next

class OutputInstruction(Instruction):
    '''
    Opcode 4 outputs the value of its only parameter. For example, the instruction 4,50
    would output the value at address 50.
    '''
    
    opcode = 4
    nargs = 1
    
    def __call__(self):
        return self[0]

class JumpIfTrueInstruction(Instruction):
    '''
    If the first parameter is non-zero, it sets the instruction pointer to the value from
    the second parameter. Otherwise, it does nothing.
    '''
    
    opcode = 5
    nargs = 2
    
    def __call__(self):
        if self[0]:
            raise self.signals.Jump(self[1])

class JumpIfFalseInstruction(Instruction):
    '''
    If the first parameter is zero, it sets the instruction pointer to the value from the
    second parameter. Otherwise, it does nothing.
    '''
    
    opcode = 6
    nargs = 2
    
    def __call__(self):
        if not self[0]:
            raise self.signals.Jump(self[1])

class LessThanInstruction(Instruction):
    '''
    If the first parameter is less than the second parameter, it stores 1 in the position
    given by the third parameter. Otherwise, it stores 0.    
    '''
    
    opcode = 7
    nargs = 3
    
    def __call__(self):
        self[2] = (self[0] < self[1]) * 1

class EqualsInstruction(Instruction):
    '''
    If the first parameter is equal to the second parameter, it stores 1 in the position
    given by the third parameter. Otherwise, it stores 0.
    '''
    
    opcode = 8
    nargs = 3
    
    def __call__(self):
        self[2] = (self[0] == self[1]) * 1

class EndInstruction(Instruction):
    opcode = 99
    nargs = 0
    
    def __call__(self):
        raise self.signals.End


class Param:
    _submap = None
    
    class errors:
        class NotFound(Exception): pass
    
    def __init__(self, value, *_):
        self.value = value
    
    def __call__(self, *args):
        if len(args) == 0:
            return self.get()
        elif len(args) == 1:
            self.set(*args)
        else:
            raise TypeError('too many args - must be one (set) or zero (get)')
    
    @classmethod
    def parse(cls, combicode, args, memory):
        nargs = len(args)
        modemask = combicode // 100
        
        for arg in args:
            modemask, mode = divmod(modemask, 10)
            
            yield cls.factory(mode, arg, memory)
    
    @classmethod
    def factory(cls, mode, value, memory):
        if cls._submap is None:
            cls._generate_submap()
        
        sub = cls._submap.get(mode)
        
        if sub:
            return sub(value, memory)
        else:
            raise cls.errors.NotFound(mode)
    
    @classmethod
    def _generate_submap(cls):
        cls._submap = {x.mode: x for x in cls.__subclasses__()}
    
    def get(self):
        raise NotImplementedError
    
    def set(self, value):
        raise NotImplementedError

class LitParam(Param):
    mode = 1
    
    def get(self):
        return self.value

class RefParam(Param):
    mode = 0
    
    def __init__(self, value, memory, *_):
        super().__init__(value)
        
        self.memory = memory
    
    def get(self):
        return self.memory[self.value]
    
    def set(self, value):
        self.memory[self.value] = value
