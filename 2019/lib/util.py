def reverse_digits(value):
    # ok, so this is really dumb
    # you would think that staying in integer land would mean better
    # performance, but this is not supported by testing
    # rather, reversing by divmodding starts faster up to about 3 digits
    # after that, converting to a string, then reversing, then converting back
    # has a much slower slowdown per digit - about 30ns per digit vs 200ns
    # my theory is that it is because much more of the logic is delegated to
    # the C Python core library - how does this shake out in PyPy?
    
    return int(''.join(reversed(str(abs(value))))) * (-1 if value < 0 else 1)

# def reverse_digits(value):
#     to_return = 0
    
#     if value < 0:
#         value *= -1
#         sign = -1
#     else:
#         sign = 1
    
#     while value:
#         value, next_digit = divmod(value, 10)
#         to_return = to_return * 10 + next_digit
    
#     return sign * to_return

def sign(value):
    return -1 if value < 0 else 1
