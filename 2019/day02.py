#!/usr/bin/env python

from lib.args import ArgumentParser as BaseArgumentParser
from lib.intcode import IntcodeEngine


'''
An Intcode program is a list of integers separated by commas (like 1,0,0,3,99).
To run one, start by looking at the first integer (called position 0). Here,
you will find an opcode - either 1, 2, or 99. The opcode indicates what to do;
for example, 99 means that the program is finished and should immediately halt.
Encountering an unknown opcode means something went wrong.

Opcode 1 adds together numbers read from two positions and stores the result in
a third position. The three integers immediately after the opcode tell you
these three positions - the first two indicate the positions from which you
should read the input values, and the third indicates the position at which the
output should be stored.

Opcode 2 works exactly like opcode 1, except it multiplies the two inputs
instead of adding them. Again, the three integers after the opcode indicate
where the inputs and outputs are, not their values.

Once you're done processing an opcode, move to the next one by stepping forward
4 positions.
'''


class ArgumentParser(BaseArgumentParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.add_argument('--target', type=int, default=19690720, help='target value for part 2')


def part1(input=None, logger=None, **__):
    '''
    Once you have a working computer, the first step is to restore the gravity
    assist program (your puzzle input) to the "1202 program alarm" state it had
    just before the last computer caught fire. To do this, before running the
    program, replace position 1 with the value 12 and replace position 2 with
    the value 2. What value is left at position 0 after the program halts?
    '''
    
    codes = load(input)
    
    return _part1(codes, logger)

def _part1(codes, logger):
    engine = IntcodeEngine(codes)
    
    engine.memory[1] = 12
    engine.memory[2] = 2
    
    list(engine.run())
    
    return engine.memory[0]

def part2(input=None, logger=None, target=None, **__):
    '''
    "With terminology out of the way, we're ready to proceed. To complete the
    gravity assist, you need to determine what pair of inputs produces the
    output 19690720."
    
    The inputs should still be provided to the program by replacing the values
    at addresses 1 and 2, just like before. In this program, the value placed in
    address 1 is called the noun, and the value placed in address 2 is called
    the verb. Each of the two input values will be between 0 and 99, inclusive.
    
    Find the input noun and verb that cause the program to produce the output
    19690720. What is 100 * noun + verb? (For example, if noun=12 and verb=2,
    the answer would be 1202.)
    '''
    
    codes = load(input)
    
    return _part2(codes, logger, target)

def _part2(codes, logger, target):
    for noun in range(100):
        for verb in range(100):
            new_codes = list(codes)
            new_codes[1:3] = noun, verb
            
            new_engine = IntcodeEngine(new_codes)
            list(new_engine.run())
            
            if new_engine.memory[0] == target:
                return 100 * noun + verb
    
    raise NotFound

def load(path):
    with path.open('r') as f:
        return [int(x) for x in f.read().strip().split(',')]

class NotFound(Exception):
    pass


if __name__=='__main__':
    ArgumentParser(2, part1, part2).do_main()
