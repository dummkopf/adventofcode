#!/usr/bin/env python

import re

from lib.args import ArgumentParser


'''
You arrive at the Venus fuel depot only to discover it's protected by a password. The
Elves had written the password on a sticky note, but someone threw it out.

However, they do remember a few key facts about the password:

- It is a six-digit number.
- The value is within the range given in your puzzle input.
- Two adjacent digits are the same (like 22 in 122345).
- Going from left to right, the digits never decrease; they only ever increase or stay
  the same (like 111123 or 135679).
'''


def part1(logger=None, start=367479, end=893698, **__):
    '''
    How many different passwords within the range given in your puzzle input
    meet these criteria?
    '''
    
    return _part1(start, end, logger)

def _part1(start, end, logger):
    valid_passwords = map(str, range(start, end+1))
    valid_passwords = filter(two_adjacent_trial, valid_passwords)
    valid_passwords = filter(non_descending_trial, valid_passwords)
    
    return len(list(valid_passwords))

def part2(logger=None, start=367479, end=893698, **__):
    '''
    An Elf just remembered one more important detail: the two adjacent matching
    digits are not part of a larger group of matching digits.
    
    Given this additional criterion, but still ignoring the range rule, the
    following are now true:

    - 112233 meets these criteria because the digits never decrease and all repeated
      digits are exactly two digits long.
    - 123444 no longer meets the criteria (the repeated 44 is part of a larger group of
      444).
    - 111122 meets the criteria (even though 1 is repeated more than twice, it still
      contains a double 22).
    '''
    
    return _part2(start, end, logger)

def _part2(start, end, logger):
    valid_passwords = map(str, range(start, end+1))
    valid_passwords = filter(two_adjacent_trial, valid_passwords)
    valid_passwords = filter(non_descending_trial, valid_passwords)
    valid_passwords = filter(strict_double_exists_trial, valid_passwords)
    
    return len(list(valid_passwords))


def two_adjacent_trial(trial_password):
    return re.search(r'(.)\1', trial_password)

def non_descending_trial(trial_password):
    return not re.search(r'(10|2[01]|3[0-2]|4[0-3]|5[0-4]|6[0-5]|7[0-6]|8[0-7]|9[0-8])', trial_password)

def strict_double_exists_trial(trial_password):
    triple_matches = set(re.findall(r'(.)\1\1', trial_password))
    double_matches = set(re.findall(r'(.)\1', trial_password))
    
    return len(triple_matches) < len(double_matches)


if __name__=='__main__':
    ArgumentParser(4, part1, part2).do_main()
