#!/usr/bin/env python

from lib.args import ArgumentParser


def part1(logger=None, input=None, **__):
    '''
    Fuel required to launch a given module is based on its mass. Specifically,
    to find the fuel required for a module, take its mass, divide by three,
    round down, and subtract 2.
    
    The Fuel Counter-Upper needs to know the total fuel requirement. To find
    it, individually calculate the fuel needed for the mass of each module
    (your puzzle input), then add together all the fuel values.

    What is the sum of the fuel requirements for all of the modules on your
    spacecraft?
    '''
    
    with input.open('r') as f:
        return _part1((int(l.strip()) for l in f), logger)

def _part1(weights, logger):
    return sum(fuel_requirement(l) for l in f)

def part2(logger=None, input=None, **__):
    '''
    What is the sum of the fuel requirements for all of the modules on your
    spacecraft when also taking into account the mass of the added fuel?
    (Calculate the fuel requirements for each module separately, then add them
    all up at the end.)
    '''
    
    with input.open('r') as f:
        return _part2((int(l.strip()) for l in f), logger)

def _part2(weights, logger):
    return sum(recursive_fuel_requirement(l, logger) for l in weights)

def fuel_requirement(module_mass):
    return module_mass // 3 - 2

def recursive_fuel_requirement(module_mass, logger):
    fuel_modules = [fuel_requirement(module_mass)]
    logger.debug(fuel_modules[-1])
    
    while fuel_modules[-1] > 0:
        fuel_modules.append(fuel_requirement(fuel_modules[-1]))
        logger.debug(fuel_modules[-1])
    
    total_fuel = sum(fuel_modules[:-1])
    
    logger.debug(f'sum: {total_fuel}')
    return total_fuel


if __name__=='__main__':
    ArgumentParser(1, part1, part2).do_main()
