#!/usr/bin/env python

from itertools import permutations, repeat, product, chain

from lib.args import ArgumentParser
from lib.intcode import IntcodeEngine


'''
Based on the navigational maps, you're going to need to send more power to your ship's
thrusters to reach Santa in time. To do this, you'll need to configure a series of
amplifiers already installed on the ship.

There are five amplifiers connected in series; each one receives an input signal and
produces an output signal. They are connected such that the first amplifier's output leads
to the second amplifier's input, the second amplifier's output leads to the third
amplifier's input, and so on. The first amplifier's input value is 0, and the last
amplifier's output leads to your ship's thrusters.

The Elves have sent you some Amplifier Controller Software (your puzzle input), a program
that should run on your existing Intcode computer. Each amplifier will need to run a copy
of the program.

When a copy of the program starts running on an amplifier, it will first use an input
instruction to ask the amplifier for its current phase setting (an integer from 0 to 4).
Each phase setting is used exactly once, but the Elves can't remember which amplifier
needs which phase setting.

The program will then call another input instruction to get the amplifier's input signal,
compute the correct output signal, and supply it back to the amplifier with an output
instruction. (If the amplifier has not yet received an input signal, it waits until one
arrives.)

Your job is to find the largest output signal that can be sent to the thrusters by trying
every possible combination of phase settings on the amplifiers. Make sure that memory is
not shared or reused between copies of the program.
'''

DAY_NUMBER = 7
LOGGER = None


def part1(input, logger, **__):
    '''
    Try every combination of phase settings on the amplifiers. What is the highest signal
    that can be sent to the thrusters?
    '''
    
    global LOGGER
    LOGGER = logger

    return _part1(load(input))

def _part1(engine):
    control_system = ThrusterControl(engine)
    
    return control_system.find_max_thrust()

def part2(**__):
    '''
    '''

    return _part2()

def _part2():
    pass

def load(input):
    with input.open() as f:
        codes = map(int, f.read().strip().split(','))
    
    return IntcodeEngine(codes)


class ThrusterControl:
    def __init__(self, engine):
        self.engine = engine
    
    def configuration(self, phase_settings):
        return ThrusterConfiguration(self.engine, phase_settings)
    
    def find_max_thrust(self):
        return max(self.configuration(x).fire() for x in permutations(range(5)))
    
class ThrusterConfiguration:
    def __init__(self, engine, phase_settings):
        self.thrusters = [Thruster(x, p) for x,p in product([engine], phase_settings)]
        
        LOGGER.debug(f'phase_settings: {phase_settings}')
    
    def fire(self):
        #implicit connections between thrusters
        
        self.thrusters[0].send(0)
        self.thrusters[0].run()
        
        for old_thruster, new_thruster in zip(self.thrusters, self.thrusters[1:]):
            new_thruster.send(old_thruster.outputs[-1])
            new_thruster.run()
        
        return new_thruster.outputs[-1]

class Thruster:
    class signals:
        class End(Exception): pass
    
    def __init__(self, engine, phase):
        self.engine = engine.copy()
        self.inputs = iter([phase])
        self.outputs = []
    
    def send(self, value):
        self.inputs = chain(self.inputs, [value])
    
    def run(self):
        self.engine.run(input=self.inputs)


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
