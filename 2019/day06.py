#!/usr/bin/env python

from collections import defaultdict

from lib.args import ArgumentParser


'''
Except for the universal Center of Mass (COM), every object in space is in orbit around
exactly one other object.

Before you use your map data to plot a course, you need to make sure it wasn't corrupted
during the download. To verify maps, the Universal Orbit Map facility uses orbit count
checksums - the total number of direct orbits (like the one shown above) and indirect
orbits.

Whenever A orbits B and B orbits C, then A indirectly orbits C. This chain can be any
number of objects long: if A orbits B, B orbits C, and C orbits D, then A indirectly
orbits D.
'''

DAY_NUMBER = 6


def part1(input, **__):
    '''
    What is the total number of direct and indirect orbits in your map data?
    '''

    return _part1(load(input))

def _part1(orbit_collection):
    return sum(len(x.ancestors) for x in orbit_collection.orbits.values())

def part2(input, logger, **__):
    '''
    Now, you just need to figure out how many orbital transfers you (YOU) need to take to
    get to Santa (SAN).
    
    You start at the object YOU are orbiting; your destination is the object SAN is
    orbiting. An orbital transfer lets you move from any object to an object orbiting or
    orbited by that object.
    
    What is the minimum number of orbital transfers required to move from the object YOU
    are orbiting to the object SAN is orbiting? (Between the objects they are orbiting -
    not between YOU and SAN.)
    '''

    return _part2(load(input), logger)

def _part2(orbit_collection, logger):
    '''
    Find the chain of orbits to transfer through to get to Santa.
    
    This translates to finding the nearest common ancestor (NCA), then finding the
    distance from NCA to each of YOU and SAN (minus one apiece).
    
    The answer is just the sum of distances.
    '''
    
    YOU = orbit_collection['YOU']
    SAN = orbit_collection['SAN']
    
    NCA = YOU.parent.common_ancestor(SAN.parent)
    logger.info(f'NCA found: {NCA}')
    
    return YOU.parent.distance_to(NCA) + SAN.parent.distance_to(NCA)

def load(input):
    with input.open() as f:
        return OrbitCollection.from_text(f.read())


class OrbitCollection:
    def __init__(self, orbits):
        self.orbits = {x.name: x for x in orbits}
        
        for orbit in self.orbits.values():
            orbit.collection = self
    
    def __getitem__(self, key):
        return self.orbits[key]
    
    @classmethod
    def from_text(cls, text):
        return cls(cls._from_text(text))
    
    @classmethod
    def _from_text(cls, text):
        ###NOTE: implemented as dual-pass so we catch the COM
        
        parent_names = defaultdict(set)
        
        for orbit_text in text.strip().split('\n'):
            stripped_text = orbit_text.strip()
            
            if stripped_text:
                parent, child = stripped_text.split(')')
                
                parent_names[parent]
                parent_names[child].add(parent)
        
        for child, parents in parent_names.items():
            if parents:
                for parent in parents:
                    yield Orbit(child, parent)
            else:
                yield Orbit(child, None)

class Orbit:
    class errors:
        class NotFound(Exception): pass
    
    def __init__(self, name, parent, collection=None):
        self.name = name
        self._parent = parent
        self.collection = collection
    
    def __str__(self):
        parent_name = self.parent and self.parent.name
        
        return f'<{self.__class__.__name__} {self.name} orbiting {parent_name}>'
    
    @property
    def parent(self):
        if self._parent and not isinstance(self._parent, Orbit):
            self._parent = self.collection.orbits[self._parent]
        
        return self._parent
    
    @property
    def ancestors(self):
        return list(self._ancestors(self))
    
    def _ancestors(self, current):
        if current.parent:
            yield current.parent
            yield from self._ancestors(current.parent)
    
    def common_ancestor(self, other):
        his_ancestors = other.ancestors
        
        for ancestor in self.ancestors:
            if ancestor in his_ancestors:
                return ancestor
        else:
            raise self.errors.NotFound
    
    def distance_to(self, other):
        current = self
        distance = 0
        
        while current:
            if current == other:
                return distance
            else:
                current = current.parent
                distance += 1
        else:
            raise self.errors.NotFound


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
