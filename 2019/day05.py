#!/usr/bin/env python

from lib.args import ArgumentParser

from lib.intcode import IntcodeEngine


'''
The Thermal Environment Supervision Terminal (TEST) starts by running a diagnostic program
(your puzzle input).

The TEST diagnostic program will start by requesting from the user the ID of the system to
test by running an input instruction - provide it 1, the ID for the ship's air conditioner
unit.

It will then perform a series of diagnostic tests confirming that various parts of the
Intcode computer, like parameter modes, function correctly. For each test, it will run an
output instruction indicating how far the result of the test was from the expected value,
where 0 means the test was successful. Non-zero outputs mean that a function is not
working correctly; check the instructions that were run before the output instruction to
see which one failed.

Finally, the program will output a diagnostic code and immediately halt. This final output
isn't an error; an output followed immediately by a halt means the program finished. If
all outputs were zero except the diagnostic code, the diagnostic program ran successfully.
'''

DAY_NUMBER = 5


def part1(input, logger, **__):
    '''
    After providing 1 to the only input instruction and passing all the tests, what
    diagnostic code does the program produce?
    '''
    
    engine = get_engine(input)
    
    return _part1(engine, logger)

def _part1(engine, logger):
    outputs = list(engine.run([1]))
    
    logger.debug(f'outputs: {outputs}')
    
    assert all(x == 0 for x in outputs[:-1])
    
    return outputs[-1]

def part2(input, logger, **__):
    '''
    This time, when the TEST diagnostic program runs its input instruction to get the ID
    of the system to test, provide it 5, the ID for the ship's thermal radiator
    controller. This diagnostic test suite only outputs one number, the diagnostic code.
    '''
    
    engine = get_engine(input)
    
    return _part2(engine, logger)

def _part2(engine, logger):
    outputs = list(engine.run([5]))
    
    logger.debug(f'outputs: {outputs}')
    
    return outputs[0]

def get_engine(input):
    with input.open() as f:
        codes = map(int, f.read().strip().split(','))
    
    return IntcodeEngine(codes)


if __name__=='__main__':
    ArgumentParser(DAY_NUMBER, part1, part2).do_main()
