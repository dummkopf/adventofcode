#!../.venv/bin/python

import re
from collections import defaultdict
from functools import partial

import constraint
import structlog

from loggy import IndentLogger, LogLevel, Format, TaggedWrapper
from util import run_as_script


'''
4 registers, initial value 0
16 opcodes

instruction: opcode input(A) input(B) output(C)
"value A" means literal value (shorthand vA)
"register A" means value at register (shorthand rA)

opcodes:
    addr:
        desc: ADD Register
        action: rA + rB -> rC
    addi:
        desc: ADD Immediate
        action: rA + vB -> rC
    mulr:
        desc: MULtiply Register
        action: rA * rB -> rC
    muli:
        desc: MULtiply Immediate
        action: rA * vB -> rC
    banr:
        desc: Bitwise ANd Register
        action: rA & rB -> rC
    bani:
        desc: Bitwise ANd Immediate
        action: rA & vB -> rC
    borr:
        desc: Bitwise OR Register
        action: rA | rB -> rC
    bori:
        desc: Bitwise OR Immediate
        action: rA | vB -> rC
    setr:
        desc: SET Register
        action: rA -> rC
    seti:
        desc: SET Immediate
        action: vA -> rC
    gtir:
        desc: Greater-Than Immediate/Register
        action: vA > rB ? 1 : 0 -> rC
    gtri:
        desc: Greater-Than Register/Immediate
        action: rA > vB ? 1 : 0 -> rC
    gtrr:
        desc: Greater-Than Register/Register
        action: rA > rB ? 1 : 0 -> rC
    eqir:
        desc: EQual Immediate/Register
        action: vA == rB ? 1 : 0 -> rC
    eqri:
        desc: EQual Register/Immediate
        action: rA == vB ? 1 : 0 -> rC
    eqrr:
        desc: EQual Register/Register
        action: rA == rB ? 1 : 0 -> rC
'''


structlog.configure(
    logger_factory=IndentLogger.factory,
    wrapper_class=TaggedWrapper,
    processors=[
        LogLevel(),
        Format('{event}')
    ],
    cache_logger_on_first_use=True
)

logger = structlog.get_logger()


def load(filename):
    return (load_samples(filename), load_instructions(filename))

def load_samples(filename):
    with open(filename, 'r') as f:
        iterf = iter(f)
        
        yield from _load_samples(iterf)

def load_instructions(filename, opnum_map):
    with open(filename, 'r') as f:
        iterf = iter(f)
        
        yield from _load_instructions(iterf, opnum_map)

def _load_samples(iterf):
    while True:
        try:
            yield parse_sample(iterf)
        except (StopIteration, BadLine):
            break

def _load_instructions(iterf, opnum_map):
    #scan forward until second section is found
    
    while True:
        try:
            maybe_yield = parse_instruction(iterf)
            
            if maybe_yield:
                O, A, B, C = maybe_yield
                
                yield partial(opcodes[opnum_map[O]], A, B, C)
        except StopIteration:
            break

def parse_sample(iterf):
    lines = []
    
    while True: #cycle until a non-blank line
        lines.append(next(iterf).strip())
        
        if lines[-1]:
            match = re.match(r'Before: \[(\d+), (\d+), (\d+), (\d+)\]', lines[-1])
            
            if match:
                before = tuple(map(int, match.groups()))
            else:
                raise BadLine(lines)
            
            
            lines.append(next(iterf).strip())
            match = re.match(r'(\d+) (\d+) (\d+) (\d+)', lines[-1])
            
            if match:
                instruction = tuple(map(int, match.groups()))
            else:
                raise MalformedSample(lines)
            
            lines.append(next(iterf).strip())
            match = re.match(r'After:  \[(\d+), (\d+), (\d+), (\d+)\]', lines[-1])
            
            if match:
                after = tuple(map(int, match.groups()))
            else:
                raise MalformedSample(lines)
            
            return before, instruction, after

def parse_instruction(iterf):
    line = next(iterf).strip()
    match = re.match(r'(\d+) (\d+) (\d+) (\d+)', line)
    
    if match:
        return tuple(map(int, match.groups()))

def part1(data=None):
    '''
    Unfortunately, while the manual gives the name of each opcode, it doesn't seem to indicate the
    number. However, you can monitor the CPU to see the contents of the registers before and after
    instructions are executed to try to work them out. Each opcode has a number from 0 through 15,
    but the manual doesn't say which is which.
    
    Ignoring the opcode numbers, how many samples in your puzzle input behave like three or more
    opcodes?
    '''
    
    three_or_more_count = 0
    
    for before, (O, A, B, C), after in load_samples(data or 'input/16'):
        if count_matching(before, after, A, B, C) >= 3:
            three_or_more_count += 1
    
    return three_or_more_count

def part2(data=None):
    '''
    Using the samples you collected, work out the number of each opcode and execute the test program (the second section of your puzzle input).

    What value is contained in register 0 after executing the test program?
    '''
    
    sample_iter = load_samples(data or 'input/16')
    
    #deduce the opnum <-> opcode map
    opnum_map = solve_opnum_map_by_constraint(sample_iter)
    
    #run program
    instruction_iter = load_instructions(data or 'input/16', opnum_map)
    return run_program(instruction_iter)

def run_program(instructions):
    registers = [0] * 4
    
    for instruction in instructions:
        instruction(registers)
    
    return registers

def solve_opnum_map_by_constraint(samples):
    possibility_map = pare_internally(samples)
    
    problem = constraint.Problem()
    
    for opnum in range(16):
        problem.addVariable(opnum, list(possibility_map[opnum]))
    
    problem.addConstraint(constraint.AllDifferentConstraint())
    
    return problem.getSolution()

def solve_opnum_map(samples):
    '''
    determine all opnum <-> opcode
      strategy:
        - for each opnum, find the intersection of every sample with it
        - find non-intersections between opnum maps iteratively until reduced to one possibility
    '''
    
    logger.debug('solve opnum map')
    
    with logger.indent():
        possibility_map = pare_internally(samples)
        return pare_crosswise(possibility_map)

def pare_internally(samples):
    logger.debug('pare internally')
    
    with logger.indent():
        possibility_map = {}
        
        for before, (opnum, A, B, C), after in samples:
            logger.debug(f'trying to identify {opnum}')
            
            with logger.indent():
                if opnum in possibility_map:
                    if len(possibility_map) > 1:
                        logger.debug('paring down')
                        
                        with logger.indent():
                            possibility_map[opnum] &= find_matching(before, after, A, B, C)
                        
                        logger.debug(f'possibility_map now {possibility_map}')
                    else:
                        logger.debug('no paring can be done')
                        
                        pass ### intentional
                else:
                    logger.debug('setting initial set')
                    
                    with logger.indent():
                        possibility_map[opnum] = find_matching(before, after, A, B, C)
                    
                    logger.debug(f'possibility_map now {possibility_map}')
        
        
        # def render_row(possibilities):
        #     def display_slot(opcode):
        #         pass
        #     pass
        
        # logger.debug('\n'.join(render_row(v) for _,v in sorted(possibility_map.items(), lambda x: x[0])))
        
        
        return possibility_map

def pare_crosswise(possibility_map):
    logger.debug('pare crosswise')
    
    with logger.indent():
        possibility_map = possibility_map.copy()
        opnum_map = {}
        
        while possibility_map:
            logger.debug('starting loop')
            
            with logger.indent():
                logger.debug(f'starting opnum_map: {opnum_map}')
                
                found = set()
                
                for k,v in ((k,v) for k,v in possibility_map.items() if len(v) == 1):
                    certainty = v.pop()
                    
                    logger.debug(f'found {k} -> {certainty}')
                    
                    found.add(certainty)
                    
                    opnum_map[k] = certainty
                
                logger.debug(f'ending opnum_map: {opnum_map}')
                
                if not found and possibility_map:
                    raise Exception('this algorithm won\'t work')
                
                to_prune = set()
                
                for opnum, possibilities in possibility_map.items():
                    possibilities -= found
                    
                    if not possibilities:
                        to_prune.add(opnum)
                
                for opnum in to_prune:
                    del possibility_map[opnum]
                
                logger.debug(f'remaining possibility map: {possibility_map}')
        
        return opnum_map


def count_matching(before, after, A, B, C):
    logger.debug(f'count opcodes that match {before}->{after}')
    
    with logger.indent():
        return len(find_matching(before, after, A, B, C))

def find_matching(before, after, A, B, C):
    '''
    strategy:
      basic: plug the "before" values into each opcode and see if the "after" matches
      optimizations:
        - registers only go up to 3, so any value greater must be literal
        - C is always the only one to change
    '''
    
    logger.debug(f'find opcodes that match {before}->{after}')
    
    with logger.indent():
        candidates = set()
        
        for name, func in opcodes.items():
            logger.debug(f'trying opcode {name}')
            
            with logger.indent():
                trial_before = list(before)
                trial_after = func(A, B, C, trial_before)
                
                if trial_after == list(after):
                    logger.debug('success!')
                    candidates.add(name)
                else:
                    logger.debug(f'failure: {trial_after} != {after}')
        
        logger.debug(f'found {len(candidates)} matching: {candidates}')
        
        return candidates

def _base(f, A, B, C, R):
    R[C] = f(A, B, R)
    
    return R

opcodes = {
    'addr': partial(_base, lambda A,B,R: R[A] + R[B]), #ADD Register
    'addi': partial(_base, lambda A,B,R: R[A] + B), #ADD Immediate
    'mulr': partial(_base, lambda A,B,R: R[A] * R[B]), #MULtiply Register
    'muli': partial(_base, lambda A,B,R: R[A] * B), #MULtiply Immediate
    'banr': partial(_base, lambda A,B,R: R[A] & R[B]), #Bitwise ANd Register
    'bani': partial(_base, lambda A,B,R: R[A] & B), #Bitwise ANd Immediate
    'borr': partial(_base, lambda A,B,R: R[A] | R[B]), #Bitwise OR Register
    'bori': partial(_base, lambda A,B,R: R[A] | B), #Bitwise OR Immediate
    'setr': partial(_base, lambda A,B,R: R[A]), #SET Register
    'seti': partial(_base, lambda A,B,R: A), #SET Immediate
    'gtir': partial(_base, lambda A,B,R: A > R[B]), #Greater-Than Immediate/Register
    'gtri': partial(_base, lambda A,B,R: R[A] > B), #Greater-Than Register/Immediate
    'gtrr': partial(_base, lambda A,B,R: R[A] > R[B]), #Greater-Than Register/Register
    'eqir': partial(_base, lambda A,B,R: A == R[B]), #EQual Immediate/Register
    'eqri': partial(_base, lambda A,B,R: R[A] == B), #EQual Register/Immediate
    'eqrr': partial(_base, lambda A,B,R: R[A] == R[B]), #EQual Register/Register
}

class _Signal(BaseException): pass
class BadLine(_Signal):
    def __init__(self, mistakenly_consumed_lines):
        super().__init__()
        self.mistakenly_consumed_lines = mistakenly_consumed_lines

class MalformedSample(Exception):
    def __init__(self, consumed_lines):
        super().__init__(f'consumed lines: {consumed_lines}')


if __name__=='__main__':
    run_as_script(15, part1, part2)
