#!/usr/bin/env python

from argparse import ArgumentParser as OG_AP
import logging

from path import Path
import pytest


logging.basicConfig()


class ArgumentParser(OG_AP):
    def __init__(self, day, *parts, **kwargs):
        super().__init__(**kwargs)
        
        self.parts = parts
        
        self.add_argument('-d', '--debug', action='store_true')
        
        self.add_argument('-v', '--verbose', action='append_const', const=1, dest='_verbosity')
        self.add_argument('-q', '--quiet', action='append_const', const=-1, dest='_verbosity')
        
        self.add_argument('-f', '--input', type=Path, default=f'input/{day:02d}', help='path to input data file')
        
        self.add_argument('--test', action='store_true', help='run test(s) instead of on real input data')
        
        self.add_argument('part', type=int)
    
    def parse_args(self, *args, **kwargs):
        kwargs['namespace'] = Namespace()
        
        return super().parse_args(*args, **kwargs)
    
    def do_main(self):
        args = self.parse_args()
        
        logger = logging.getLogger()
        logger.setLevel(logging.ERROR - 10*args.verbosity)
        
        if args.test:
            pytest.main()
        else:
            try:
                print(self.parts[args.part - 1](logger=logger, **vars(args)))
            except:
                if args.debug:
                    import ipdb
                    ipdb.post_mortem()
                else:
                    raise


class Namespace:
    @property
    def verbosity(self):
        return sum(self._verbosity or [])
