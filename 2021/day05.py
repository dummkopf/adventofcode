#!/usr/bin/env python

from collections import namedtuple
import re

from lib.main import DayRunner, ParseException


class Day05Runner(DayRunner):
	DAY = 5

	@classmethod
	def parse(cls, file):
		return list(cls._parse(file))

	@classmethod
	def _parse(cls, file):
		for line in file:
			line = line.strip()

			match = re.fullmatch(r'(?P<x1>\d+),(?P<y2>\d+) -> (?P<x2>\d+),(?P<y2>\d+)', line)

			if match:
				g = lambda x: int(match.group(x))
				yield (g('x1'),g('y1')), (g('x2'),g('y2'))
			else:
				raise ParseException(f'line didn\'t match: {line!r}')

	def munge_in(self, raw_data):
		self.lines = {Line(Point(x1,y1), Point(x2,y2)) for (x1,y1),(x2,y2) in raw_data}

class Day05Part1Runner(Day05Runner):
	PART = 1

	def run(self):
		pass

class Day05Part1Runner(Day05Runner):
	PART = 2

	def run(self):
		pass

Point = namedtuple('Point', 'x y')

class Line:
	def __init__(self, p1, p2):
		self.p1 = p1
		self.p2 = p2

	@property
	def is_orthogonal(self):
		return self.is_horizontal or self.is_vertical

	@property
	def is_horizontal(self):
		return self.ydiff == 0

	@property
	def is_vertical(self):
		return self.xdiff == 0

	@property
	def pslope(self):
		return self.xdiff, self.ydiff

	@property
	def xdiff(self):
		return self.p2.x - self.p1.x

	@property
	def ydiff(self):
		return self.p2.y - self.p1.y

	@classmethod
	def overlaps(cls, lines):
		pass

	def parallel_with(self, other):
		return self.xdiff * other.ydiff == self.ydiff * other.xdiff

	def inline_with(self, other):
		return self.parallel_with(other) and self.ydiff*(other.p1.x-self.p1.x) == self.xdiff*(other.p1.y-self.p1.y)

	def overlap_with(self, other):
		pass

if __name__ == '__main__':
	print(Day05Runner.main().run())
