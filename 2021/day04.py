#!/usr/bin/env python

from collections import defaultdict
import re

from lib.main import DayRunner
from lib.parse import chomp_blank_lines, parse_table, EOF


class Day04Runner(DayRunner):
	DAY = 4

	@classmethod
	def parse(cls, file):
		# expecting a single line with comma-separated integers
		draw_order = [int(x) for x in file.readline().split(',')]

		# expecting a number of...
		boards = []

		while True:
			try:
				# expecting a number > 0 of blank lines
				chomp_blank_lines(file, mincount=1)

				# expecting an NxN table of integers, columns space+delimited and rows newline-delimited
				boards.append(parse_table(file))
			except EOF:
				break

		return draw_order, boards

	def munge_in(self, raw_data):
		self.draw_order, boards_data = raw_data

		self.boards = [Board(board_data) for board_data in boards_data]

class Day04Part1Runner(Day04Runner):
	PART = 1

	def run(self):
		for draw in self.draw_order:
			for board in self.boards:
				board.mark(draw)

				# presume that only one board will win
				if board.won:
					return board.score

class Day04Part2Runner(Day04Runner):
	PART = 2

	def run(self):
		boards_left = set(self.boards[:])

		for draw in self.draw_order:
			boards_to_remove = set()

			for board in boards_left:
				board.mark(draw)

				if board.won:
					boards_to_remove.add(board)

			boards_left -= boards_to_remove

			if not boards_left:
				break

		print(f'{board.score}: {board.last_mark} * {board.unmarked_cells} ({sum(board.unmarked_cells)})')
		return board.score

class Board:
	def __init__(self, raw_data):
		self.rows = defaultdict(dict)
		self.cols = defaultdict(dict)
		self.addresses = {}
		self.last_mark = None

		for (x,y),v in raw_data.items():
			self.addresses[v] = x,y

			# link to row and col
			self.rows[y][x] = False
			self.cols[x][y] = False

	def mark(self, value):
		if value in self.addresses:
			x,y = self.addresses[value]

			self.rows[y][x] = True
			self.cols[x][y] = True

			self.last_mark = value

	@property
	def won(self):
		return any(all(col.values()) for col in self.cols.values()) or any(all(row.values()) for row in self.rows.values())

	@property
	def unmarked_cells(self):
		return [k for k,(x,y) in self.addresses.items() if not self.rows[y][x]]

	@property
	def score(self):
		return self.last_mark * sum(self.unmarked_cells)


if __name__ == '__main__':
	print(Day04Runner.main().run())
