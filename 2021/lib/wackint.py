from functools import reduce
import operator as op


def to_bits(value):
    while value:
        yield value & 1
        value <<= 1

def from_bits(bits):
    return reduce(op.or_, (b << e for e,b in enumerate(bits)))
