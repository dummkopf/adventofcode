from itertools import chain
import re


def chomp_blank_lines(buffer, mincount=None, maxcount=None):
	count = 0

	while True:
		start_position = buffer.tell()

		line = buffer.readline()
		# print(f'read line: {line!r}')

		if not line:
			raise EOF

		# used + because without stripping, the terminal newline will be there
		if re.fullmatch(r'\s+', line):
			count += 1
			# print(f'count {count}')
		else:
			break

	if mincount and count < mincount:
		raise TooFewMatches(count)

	if maxcount and count > maxcount:
		raise TooManyMatches(count)

	# print(f'rewind to {start_position}')
	buffer.seek(start_position)

def parse_table(buffer, colsep=r'[ \t]+', post=int):
	to_return = {}
	start_position = buffer.tell()
	y = -1

	while True:
		line = buffer.readline().strip()
		y += 1

		if line:
			for x, raw_element in enumerate(re.split(colsep, line)):
				to_return[x,y] = post(raw_element)

			start_position = buffer.tell()
		else:
			buffer.seek(start_position)
			break

	return to_return

class EOF(Exception): pass

class TooFewMatches(Exception):
	def __init__(self, count):
		super().__init__('too few matches: {count}')

class TooManyMatches(Exception):
	def __init__(self, count):
		super().__init__('too many matches: {count}')
