from argparse import ArgumentParser
import ipdb
import re
import sys

from path import Path


def pdb_hook(type, value, traceback):
	print(f'{type}({value})')
	ipdb.post_mortem(traceback)


class DayRunner:
	DAY = None
	PART = None

	def __init__(self, data, debug=False):
		self.DEBUG = debug

		self.munge_in(data)

	@classmethod
	def main(cls):
		args = cls.getargs()

		if args.pdb:
			sys.excepthook = pdb_hook

		if args.input_file:
			input_file = args.input_file.open('r')
		elif args.sample_data:
			input_file = Path(f'input/{cls.DAY:02d}.sample').open('r')
		elif args.real_data:
			input_file = Path(f'input/{cls.DAY:02d}').open('r')
		else:
			input_file = sys.stdin

		try:
			parsed_data = cls.parse(input_file)
		finally:
			input_file.close()

		partcls = next(x for x in cls.__subclasses__() if f'Part{args.part}' in x.__name__)

		return partcls(parsed_data, debug=args.debug)

	@classmethod
	def getargs(cls):
		parser = ArgumentParser()
		parser.add_argument('part', type=int)

		source_meg = parser.add_mutually_exclusive_group()
		source_meg.add_argument('--sample-data', '--sample', action='store_true')
		source_meg.add_argument('--real-data', '--real', action='store_true')
		source_meg.add_argument('--input-file', '-f', type=Path)

		parser.add_argument('--debug', action='store_true')
		parser.add_argument('--pdb', action='store_true')

		args = parser.parse_args()

		return args

	@classmethod
	def parse(cls, buffer):
		pass

	def munge_in(self, raw_data):
		pass

	def run(self):
		pass


class ParseException(Exception):
	pass
