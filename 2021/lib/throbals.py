import threading

from .frame import caller


THREAD_LOCAL = threading.local()


def get_throbal(name, frame_depth=1):
	try:
		return getattr(THREAD_LOCAL, name)
	except AttributeError:
		return caller(frame_depth).f_globals[name]

def set_throbal(name, value, frame_depth=1):
	caller(frame_depth).f_globals[name] = value
	setattr(THREAD_LOCAL, name, value)

	return value

def del_throbal(name, frame_depth=1):
	del caller(frame_depth).f_globals[name]
	delattr(THREAD_LOCAL, name)

class ThreadGlobalHelper:
	def __getattr__(self, name):
		return get_throbal(name, frame_depth=2)

	def __setattr__(self, name, value):
		set_throbal(name, value, frame_depth=2)

	def __delattr__(self, name, frame_depth=2):
		del_throbal(name)

throbals = ThreadGlobalHelper()

def def_throbal(f):
	return set_throbal(f.__name__, f, frame_depth=2)
