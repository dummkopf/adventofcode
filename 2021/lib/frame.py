from inspect import currentframe


def caller(frame_depth=1):
	current_frame = currentframe()

	for i in range(frame_depth):
		current_frame = current_frame.f_back

	return current_frame
