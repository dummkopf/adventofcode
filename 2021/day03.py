#!/usr/bin/env python

import argparse
import operator as op

from path import Path

from lib.wackint import from_bits


DAY = 3


def getargs():
    parser = argparse.ArgumentParser()

    parser.add_argument('-f', '--input', type=Path, default=f'input/{DAY:02d}', help='path to input data file')
    parser.add_argument('-p', '--part', type=int)

    return parser.parse_args()

def munge(args):
    newargs = {}

    with args.input.open('r') as f:
        newargs['intdata'] = [int(x.strip(), 2) for x in f if x]

    return dict(**args.__dict__, **newargs)

def main(*, part, **rest):
    if part == 1:
        return part1(**rest)
    elif part == 2:
        return part2(**rest)
    else:
        raise Exception('you\'re smoking something')

def part1(*, intdata, **rest):
    '''
    gamma rate is the most-common bit per position of the report
    epsilon rate is the converse
    return the product
    '''

    max_bits = max(x.bit_length() for x in intdata)

    return find_gamma_rate(intdata, max_bits) * find_epsilon_rate(intdata, max_bits)

def part2(*, intdata, **rest):
    '''
    Use the binary numbers in your diagnostic report to calculate the oxygen
    generator rating and CO2 scrubber rating, then multiply them together.
    What is the life support rating of the submarine?

    find O2G rating by filtering data by whether each successive (big-endian)
     bit is equal to the most common for that position, defaulting to 1
    converse for CO2 rating
    '''

    max_bits = max(x.bit_length() for x in intdata)

    return find_o2_rating(intdata, max_bits) * find_co2_rating(intdata, max_bits)

def count_bits(intdata, max_bits):
    return [sum(bool(value & (1 << i)) for value in intdata) for i in range(max_bits)]

def find_gamma_rate(intdata, max_bits):
    break_even = len(intdata) // 2 + (len(intdata) & 1)
    sums = count_bits(intdata, max_bits)

    return from_bits(s >= break_even for s in sums)

def find_epsilon_rate(intdata, max_bits):
    break_even = len(intdata) // 2
    sums = count_bits(intdata, max_bits)

    print(f'intdata {intdata}')
    print(f'break even {break_even}')
    print(f'sums {sums!r}')

    return from_bits(s <= break_even for s in sums)

def find_o2_rating(intdata, max_bits):
    return find_rating(intdata, max_bits, op.ge)

def find_co2_rating(intdata, max_bits):
    return find_rating(intdata, max_bits, op.lt)

def find_rating(intdata, max_bits, discriminator):
    remaining = intdata[:]

    for bit in range(max_bits-1, -1, -1):
        print(f'working on bit {bit}')
        mask = 1 << bit
        popcount = sum((x & mask) >> bit for x in remaining)
        target = discriminator(popcount, len(remaining) / 2)
        print(f'  targeting {int(target)}')

        remaining = [x for x in remaining if (x & mask) >> bit == target]
        print('  remaining:{}'.format(''.join(f'\n    {bin(x)}' for x in remaining)))

        if len(remaining) == 1:
            return remaining[0]
    else:
        raise Exception('this should be impossible')


if __name__=='__main__':
    print(main(**munge(getargs())))
