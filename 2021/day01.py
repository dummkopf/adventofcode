#!/usr/bin/env python


import argparse

from path import Path


DAY = 1


def getargs():
    parser = argparse.ArgumentParser()

    parser.add_argument('-f', '--input', type=Path, default=f'input/{DAY:02d}', help='path to input data file')
    parser.add_argument('-p', '--part', type=int)

    return parser.parse_args()

def munge(args):
    newargs = {}

    with args.input.open('r') as f:
        newargs['depths'] = [int(x.strip()) for x in f]

    return dict(**args.__dict__, **newargs)

def main(*, part, **rest):
    ###TODO: refactor as OO
    #        class Day - include std args, munging, and main
    #        class Day01(Day) - include just parts
    #        include Day.run(day, part)
    #        include option for copying to clipboard

    if part == 1:
        return part1(**rest)
    elif part == 2:
        return part2(**rest)
    else:
        raise Exception('you\'re smoking something')

def part1(*, depths, **rest):
    '''
    To do this, count the number of times a depth measurement increases from the previous measurement.
    (There is no measurement before the first measurement.)
    '''

    return sum(a < b for a,b in zip(depths, depths[1:]))

def part2(*, depths, **rest):
    '''
    Your goal now is to count the number of times the sum of measurements in this sliding window increases
    from the previous sum. So, compare A with B, then compare B with C, then C with D, and so on. Stop when
    there aren't enough measurements left to create a new three-measurement sum.
    '''

    windowed_depths = list(zip(depths, depths[1:], depths[2:]))

    return sum(sum(a) < sum(b) for a,b in zip(windowed_depths, windowed_depths[1:]))


if __name__=='__main__':
    print(main(**munge(getargs())))
