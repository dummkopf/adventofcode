#!/usr/bin/env python


from functools import reduce
import argparse
import operator as op

from path import Path

from lib.vector import Vector


DAY = 2


def getargs():
    parser = argparse.ArgumentParser()

    parser.add_argument('-f', '--input', type=Path, default=f'input/{DAY:02d}', help='path to input data file')
    parser.add_argument('-p', '--part', type=int)

    return parser.parse_args()

def munge(args):
    newargs = {}

    return dict(**args.__dict__, **newargs)

def main(*, part, **rest):
    if part == 1:
        return part1(**rest)
    elif part == 2:
        return part2(**rest)
    else:
        raise Exception('you\'re smoking something')

def part1(*, input, **rest):
    '''
    Calculate the horizontal position and depth you would have after following the planned course.
    What do you get if you multiply your final horizontal position by your final depth?
    '''

    ship = Ship_v1()

    with input.open('r') as f:
        instructions = [Ship_v1.parse_line(l) for l in f]

    for instruction in instructions:
        ship.move(instruction)

    return reduce(op.mul, ship.position, 1)

def part2(*, input, **rest):
    '''
    In addition to horizontal position and depth, you'll also need to track a third value, aim,
    which also starts at 0. The commands also mean something entirely different than you first thought:
        - down X increases your aim by X units.
        - up X decreases your aim by X units.
        - forward X does two things:
            - It increases your horizontal position by X units.
            - It increases your depth by your aim multiplied by X.

    Using this new interpretation of the commands, calculate the horizontal position and depth you
    would have after following the planned course. What do you get if you multiply your final
    horizontal position by your final depth?
    '''

    ship = Ship_v2()

    with input.open('r') as f:
        instructions = [Ship_v2.parse_line(l) for l in f]

    for instruction in instructions:
        instruction(ship)

    return reduce(op.mul, ship.position, 1)


class Ship_v1:
    def __init__(self):
        self.position = Vector(0,0)

    @classmethod
    def parse_line(cls, line):
        instruction, amount = line.strip().split(' ')
        amount = int(amount)

        if instruction == 'forward':
            vector = Vector(1, 0)
        elif instruction == 'down':
            vector = Vector(0, 1)
        elif instruction == 'up':
            vector = Vector(0, -1)
        else:
            raise Exception(f'whatchu talkin about with your so-called "{line}"')

        return amount * vector

    def move(self, vector):
        self.position += vector


class Ship_v2(Ship_v1):
    def __init__(self):
        super().__init__()

        self.aim = 0

    @classmethod
    def parse_line(cls, line):
        instruction, amount = line.strip().split(' ')
        amount = int(amount)

        if instruction == 'down':
            return lambda ship: ship.steer(amount)
        elif instruction == 'up':
            return lambda ship: ship.steer(-amount)
        elif instruction == 'forward':
            return lambda ship: ship.move(amount * Vector(1, ship.aim))
        else:
            raise Exception(f'whatchu talkin about with your so-called "{line}"')

    def steer(self, amount):
        self.aim += amount


if __name__=='__main__':
    print(main(**munge(getargs())))
