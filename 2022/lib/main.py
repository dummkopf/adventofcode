from argparse import ArgumentParser
from collections import deque
import ipdb
import re
import sys

from path import Path
import crayons as color

from .package import touch


def pdb_hook(type, value, traceback):
	print(f'{type}({value})')
	ipdb.post_mortem(traceback)


class DayRunner:
	DAY = None
	PART = None

	def __init__(self, data, debug=False):
		self.DEBUG = debug

		self.munge_in(data)

	@classmethod
	def _registry_key(cls):
		return cls.DAY, cls.PART

	@classmethod
	def main(cls):
		args = cls.getargs()

		if args.pdb:
			sys.excepthook = pdb_hook

		touch('days')

		# not done with a str.join so each value gets printed before the very end
		for rv in args.func(args):
			print(rv)

	@classmethod
	def show_list(cls, args):
		for partcls in cls._registry.values():
			# collapse whitespace, remove leading and trailing whitespace, and limit length
			doc = partcls.__doc__
			doc = re.sub(r'\s{2,}', ' ', doc).strip()

			if len(doc) > 100:
				doc = doc[:95] + '[...]'

			yield f'{partcls.DAY:02d} {partcls.PART:02d} {doc}'

		###TODO: support --test

	@classmethod
	def show_details(cls, args):
		partcls = cls._registry[args.day, args.part]

		yield f'{partcls.DAY:02d} {partcls.PART:02d}'
		yield partcls.__doc__

		###TODO: support --test
		###TODO: display module docstring too

	@classmethod
	def _main(cls, args):
		partcls = cls._registry[args.day, args.part]

		for instance, expected_value, input_path in cls.find_paths(partcls, args.sample_data):
			input_file = input_path.open('r')
			
			try:
				parsed_data = partcls.parse(input_file)

				runner = partcls(parsed_data, debug=args.debug)

				if expected_value is None:
					yield from runner.run()
				else:
					yield instance

					output_buffer = deque(maxlen=1)

					for output_value in runner.run():
						output_buffer.append(output_value)
						yield output_value

					yield color.green('pass', bold=True) if str(output_buffer.pop()) == expected_value else (f'{expected_value}:' + color.red('fail', bold=True))
			finally:
				input_file.close()

	@classmethod
	def find_paths(cls, partcls, sample:bool):
		if sample:
			candidate_paths = Path('input').glob(f'{partcls.DAY:02d}.{partcls.PART:1d}.*sample*')
			valid_paths = []

			for path in candidate_paths:
				match = re.fullmatch(r'(?P<day>\d{2})\.(?P<part>\d)(?:\.(?P<instance>\d+))?\.sample(?::(?P<expected>.+?))?', path.name)

				if match:
					valid_paths.append((
						match.group('instance') and int(match.group('instance')),
						match.group('expected'),
						Path('input') / match.string,
					))

			if not valid_paths:
				raise NoValidFiles(partcls.DAY, partcls.PART, candidate_paths)

			yield from sorted(valid_paths)
		else:
			yield None, None, Path(f'input/{partcls.DAY:02d}')

	@classmethod
	def getargs(cls):
		parser = ArgumentParser()
		parser.add_argument('--debug', action='store_true')
		parser.add_argument('--pdb', action='store_true')

		subparsers = parser.add_subparsers(dest='mode')
		parser.set_defaults(mode='run')

		default_parser = subparsers.add_parser('run')
		default_parser.set_defaults(func=cls._main)
		default_parser.add_argument('day', type=int)
		default_parser.add_argument('part', type=int)

		default_parser.add_argument('--sample-data', '--sample', action='store_true')

		list_parser = subparsers.add_parser('list')
		list_parser.set_defaults(func=cls.show_list)
		list_parser.add_argument('--test', action='store_true')

		show_parser = subparsers.add_parser('show')
		show_parser.set_defaults(func=cls.show_details)
		show_parser.add_argument('day', type=int)
		show_parser.add_argument('part', type=int)

		return parser.parse_args()

	@classmethod
	def parse(cls, buffer):
		for line in buffer:
			line = line.rstrip('\n')

			if line:
				yield from cls.parse_line(line)

	@classmethod
	def parse_line(cls, line):
		yield line

	def munge_in(self, parsed_data):
		pass

	def run(self):
		pass


class ParseException(Exception): pass
class NoValidFiles(Exception):
	def __init__(self, day, part, candidates):
		super().__init__(f'no candidate input paths panned out for day:{day:02d},part:{part}: {",".join(map(repr, candidates))}')
