'''
Encode binary output so it can be tested against.
'''

from functools import reduce
from itertools import chain
import operator as op


def encode_line(bits, concrete=False, raw=False):
	'''
	Encode a line of bits, represented as an iterable of bools.
	'''

	bits = iter(bits)

	def generate_bytes():
		while True:
			chunk = [x for _,x in zip(range(8), bits)]

			if chunk:
				yield intify_bits(chunk, little=True)
			else:
				break

	rvgen = (chr((1-raw)*256+x) for x in generate_bytes())

	if concrete:
		return ''.join(rvgen)
	else:
		return rvgen

def encode_lines(lines, concrete=False, raw=False):
	lines = iter(lines)
	linesep = '\u0100' if raw else '\u0200'

	linegen = (encode_line(l, concrete=False, raw=raw) for l in lines)

	def generate_lines():
		for line in linegen:
			yield from line
			yield linesep

	if concrete:
		return ''.join(generate_lines())
	else:
		return generate_lines()

def intify_bits(bits, little=True):
	if not little:
		bits = reversed(list(bits))

	return reduce(op.add, (bool(f)*2**i for i,f in enumerate(bits)), 0)

def encode_aoc_bool_grid(text, on_char='#'):
	return encode_lines(((c == on_char for c in line) for line in text.split('\n')), concrete=True)
