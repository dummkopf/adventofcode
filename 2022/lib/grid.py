class Sentinel: pass
class NOT_PROVIDED(Sentinel): pass

class Grid(dict):
	def __init__(self, flat_data):
		super().__init__()

		self.minx = self.maxx = self.miny = self.maxy = None

		for y,row in enumerate(flat_data):
			for x,value in enumerate(row):
				self[x,y] = value

	def __repr__(self):
		srepr = super().__repr__()

		return f'<{self.__class__.__name__} {srepr}>'

	def __str__(self):
		return '\n'.join(''.join(str(self.get((x,y))) for x in range(self.minx, self.maxx+1)) for y in range(self.miny, self.maxy+1))

	def __getitem__(self, key):
		x,y = key

		return super().__getitem__((x,y))

	def __setitem__(self, key, value):
		x,y = key

		self.minx = x if self.minx is None else min(x, self.minx)
		self.maxx = x if self.maxx is None else max(x, self.maxx)
		self.miny = y if self.miny is None else min(y, self.miny)
		self.maxy = y if self.maxy is None else max(y, self.maxy)

		super().__setitem__(key, value)

	def __contains__(self, key):
		x,y = key

		return self.minx <= x <= self.maxx and self.miny <= y <= self.maxy

	def get(self, key, default=None):
		# overriding because stdlib is doing something weird here

		try:
			return self[key]
		except KeyError:
			return default

	def keys(self):
		return ((x,y) for x in range(self.minx, self.maxx+1) for y in range(self.miny, self.maxy+1))

	def get_ray(self, start, vector, inclusive=False, default=NOT_PROVIDED):
		current = start + (not inclusive) * vector
		
		while current in self:
			if default is NOT_PROVIDED:
				yield self.get(current, default)
			else:
				yield self[current]

			current = current + vector
