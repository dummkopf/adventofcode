from collections import deque
from itertools import repeat

from .package import register


class Sentinel: pass
class Stack(Sentinel): pass

class CPU:
	INSTRUCTION_SET:dict

	def __init__(self):
		# filled in later by parse()
		self.program = []

		self.pc = 0
		self.time = 0
		self.stack = deque()
		self.registers = {}

	def run(self, tail_noop=False):
		# position 0 is significant
		yield None

		while self.pc < len(self.program):
			for _ in self.program[self.pc].run():
				self.time += 1
				yield None

		if tail_noop:
			while True:
				self.time += 1
				yield None

	def parse(self, line):
		name, *args = line.split(' ')

		instruction_cls = self.INSTRUCTION_SET[name]

		return instruction_cls(self, *args)

	@classmethod
	def register(cls):
		return register(cls, key=lambda o: o.NAME, container='INSTRUCTION_SET')

class Instruction:
	NAME = None
	DELAY = 1
	TARGET = Stack

	def __init__(self, cpu):
		self.cpu = cpu

	@property
	def result(self):
		pass

	def jump(self):
		self.cpu.pc += 1

	def run(self):
		yield from repeat(None, self.DELAY-1)

		if self.TARGET is None:
			pass
		elif self.TARGET is Stack:
			self.cpu.stack.append(self.result)
		else:
			self.cpu.registers[self.TARGET] = self.result

		self.jump()

		yield None
