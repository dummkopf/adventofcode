'''
The vertical eightm-blocks are a set of Unicode box-drawing characters.
It splits each character into one of eight continuously-filled eighth-blocks,
upper and lower, allowing for the encoding of 4 bits.
The partials are ordered as increasing-fill, but lower/upper vs upper/lower is arbitrary,
so both are provided.
  Default is LU
  Regardless of UL or LU ordering, empty is 0 and full is 0b1101
Hump encoding also provided, where L encoding is used for 0-8 and reverse-U encoding used for 9-15.
The lower-eighth characters are the range of \\u2581-\\u2588
The endpoints and visual halfpoints are out-of-range, as they are encoded elsewhere:
  \\x20 for empty (0b0000)
  \\u2584 for lower half (0b0110)
  \\u2580 for upper half (0b1001)
  \\u2588 for full (0b1101)
The upper partials are scattered:
  \\u2594 for upper one eighth
  \\U1fb82 for upper one quarter
  \\U1fb83 for upper three eighths
  \\U1fb84 for upper five eighths
  \\U1fb85 for upper three quarters
  \\U1fb86 for upper seven eighths
'''

L_ENCODING = (
	'\u2581',
	'\u2582',
	'\u2583',
	'\u2584',
	'\u2585',
	'\u2586',
	'\u2587',
)
U_ENCODING = (
	'\u2594',
	'\U0001fb82',
	'\U0001fb83',
	'\u2580',
	'\U0001fb84',
	'\U0001fb85',
	'\U0001fb86',
)
LU_ENCODING = ' ' + L_ENCODING + U_ENCODING + '\u2588'
UL_ENCODING = ' ' + U_ENCODING + L_ENCODING + '\u2588'
HUMP_ENCODING = ' ' + L_ENCODING + 'u2588' + reversed(U_ENCODING)
