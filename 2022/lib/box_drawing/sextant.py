'''
Block Sextant is a Unicode range of legacy-supporting box-drawing characters.
It splits each character into one of each combination of six (2x3) segments,
allowing for representing a 6-bit value visually per character.
The segments are ordered:
  0 1
  2 3
  4 5
The endpoints (encoded value 0 and 63) are special, as they already had
representation: \\x20 and \\u2588, respectively.
'''
