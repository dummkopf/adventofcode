'''
Block Quadrant is a Unicode range of box-drawing characters.
It splits each character into one of each combination of four (2x2) segments,
allowing for representing a 4-bit value visually per character.
The segments are ordered:
  0 3
  1 2
An alternate ordering is provided to align with Block Sextant.
The endpoints and visual halfpoints are out-of-range, as they are encoded elsewhere:
  \\x20 for empty (0b0000)
  \\u2588 for full (0b1111)
  \\u2584 for lower half (0b0110)
  \\u2580 for upper half (0b1001)
  \\u258c for left half (0b1100)
  \\u2590 for right half (0b0011)
'''
