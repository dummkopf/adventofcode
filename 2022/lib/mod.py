def egcd(a, b):
	'''
	Calculate gcd plus Bezout's identity via Extended Euclidean algorithm.

	ax + by = gcd(a,b)
	'''

	m,n,u,v = 1,0,0,1

	if not b:
		return a,u,v

	while True:
		a,(q,b) = b,divmod(a,b)

		if b:
			m,u = u, m-q*u
			n,v = v, n-q*v
		else:
			return a,u,v

def modcong(a, b, n):
	'''
	Test congruency modulo n with only one modulo operation.
	'''

	return (a - b) % n == 0

def chinrem(a, b, m, n):
	'''
	For any a, b and coprime m, n, there exists a unique x (mod mn) such that x ≡ a (mod m)
	and x ≡ b (mod n). In fact, x ≡ b mn–1 m + a nm–1 n (mod mn) where mn−1 is the inverse
	of m modulo n and nm−1 is the inverse of n modulo m.
	'''

	return (b*modmulinv(m, n)*m + a*modmulinv(n, m)*n) % (m*n)

def modmulinv(a, n):
	'''
	Modular multiplicative inversion, via Extended Euclidean algorithm.
	'''

	g,x,y = egcd(a, n)

	assert g == 1

	return x
