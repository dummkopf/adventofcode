class Range:
	def __new__(cls, first, last):
		if first <= last:
			return object.__new__(cls)
		else:
			return NullRange.__new__(NullRange)

	def __init__(self, first, last):
		self.first = first
		self.last = last

	def __ge__(self, other):
		return self.first <= other.first and self.last >= other.last

	def __and__(self, other):
		if self.first < other.first:
			return self.__class__(other.first, self.last)
		else:
			return self.__class__(self.first, other.last)

class NullRange(Range):
	_singleton = None

	def __new__(cls):
		if not cls._singleton:
			cls._singleton = object.__new__(NullRange)

		return cls._singleton

	def __init__(self, *a, **k):
		pass #intentional

	def __ge__(self, other):
		return False

	def __and__(self, other):
		return self

	def __bool__(self):
		return False
