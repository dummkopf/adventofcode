class ChangeSet(dict):
	def __setitem__(self, key, value):
		if isinstance(value, BaseChange):
			return super().__setitem__(key, value)
		else:
			return self.set(key, value)
	__setattr__=__setitem__

	def __getattr__(self, key):
		return super().__getitem__(key)

	def __delattr__(self, key):
		return super().__delitem__(key)

	@classmethod
	def diff(cls, before, after, kind=None):
		kind = kind or AbsoluteChange

		pass ###TODO

	def apply(self, obj):
		for key,change in self.items():
			before = getattr(obj, key)
			after = change.apply(before, obj)

			setattr(obj, key, after)

	def set(self, key, value):
		return setattr(self, key, AbsoluteChange(value))

	def change(self, key, value):
		before = getattr(self, key)

		return setattr(self, key, RelativeChange.diff(before, value))

	@property
	def reversed(self):
		pass


class BaseChange:
	'''
	I am the Senate.

	IMPORTANT: agnostic of what attr it applies to
	'''

	def __init__(self, value):
		self.value = value

	@classmethod
	def diff(self, before, after):
		pass

	def apply(self, before, obj):
		'''
		before: the before *value*
		obj: the object in which the attr exists

		IMPORTANT: obj is only for reference, not for mutation
		'''

		pass

class AbsoluteChange(BaseChange):
	'''
	Just clobber.
	'''

	@classmethod
	def diff(cls, before, after):
		return cls(after)

	def apply(self, before, obj):
		return self.value

class RelativeChange(BaseChange):
	'''
	Only the amount changed.

	IMPORTANT: The attr's type needs to have __sub__ and __add__ defined.
	'''

	@classmethod
	def diff(cls, before, after):
		return cls(after - before)

	def apply(self, before, obj):
		return before + self.value

class NoChange(BaseChange):
	@classmethod
	def diff(cls, before, after):
		return cls()
