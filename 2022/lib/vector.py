from collections.abc import Sequence, Iterable
from itertools import repeat


class Vector(Sequence):
	def __init__(self, *elements):
		if len(elements) == 1 and isinstance(elements[0], Iterable):
			self._values = tuple(elements[0])
		else:
			self._values = tuple(elements)

	def __str__(self):
		return f'<{self.__class__.__name__} {self._values!r}>'
	__repr__=__str__

	def __getitem__(self, key):
		return self._values[key]

	def __len__(self):
		return len(self._values)

	def __rmul__(self, factor):
		return self.__class__(factor*x for x in self)

	def __add__(self, other):
		assert len(self) == len(other)

		return self.__class__(x+y for x,y in zip(self, other))
	__radd__=__add__

	def __sub__(self, other):
		other = self.__class__(other)

		return self + -1*other

	def __rsub__(self, other):
		return -1*self + other

	def __abs__(self):
		return self.__class__(map(abs, self))

	def __bool__(self):
		return any(self)

	def __eq__(self, other):
		return tuple(self) == tuple(other)

	def __hash__(self):
		return hash(self.__class__) ^ hash(self._values)

	@property
	def unit(self):
		return 1/self.rss * self

	@property
	def is_unit(self):
		return self.rss == 1

	@property
	def is_ortho(self):
		# one-hot detection
		#KLUGE: getting a length on an iterator
		return sum(map(bool, self)) == 1

	@property
	def rss(self):
		'''
		Root sum square, aka hypotenuse for 2-vector
		'''

		return sum(x**2 for x in self)**0.5

	@property
	def manhattan_distance(self):
		return sum(map(abs, self))

	@property
	def ring_distance(self):
		diagonal_steps = min(map(abs, self))
		remainder = abs(self) - self.__class__(repeat(diagonal_steps, len(self)))

		return diagonal_steps + remainder.manhattan_distance

	@property
	def cardinal_direction(self):
		'''
		Each component can have one of: -1, 0, 1
		Corresponds to 8-point cardinal flower.
		'''

		return self.__class__(x and 2*(x>0)-1 for x in self)
