from lib.cpu import CPU, Instruction
from lib.main import DayRunner
from lib.package import register
from lib.words import encode_lines


class Signal(Exception): pass

class Error(Exception): pass

class Sentinel: pass


class Day10(DayRunner):
	'''
	'''

	DAY = 10

	def munge_in(self, parsed_data):
		self.device = CommDevice()
		self.device.program = [self.device.parse(line) for line in parsed_data]

@register(DayRunner)
class Part1(Day10):
	'''
	Find the signal strength during the 20th, 60th, 100th, 140th, 180th, and 220th cycles. What is the sum of these six signal strengths?
	'''

	PART = 1

	def run(self):
		total = 0
		runner = self.device.run()

		for delay in [20,40,40,40,40,40]:
			if self.DEBUG:
				for _ in zip(range(delay), runner):
					yield f' {self.device.registers["X"]}'
				yield f'={self.device.signal_strength}'
			else:
				list(zip(range(delay), runner))

			total += self.device.signal_strength

		yield total


@register(DayRunner)
class Part2(Day10):
	'''
	Racing the beam

	Screen is 40x6

	It seems like the X register controls the horizontal position of a sprite. Specifically, the sprite is 3 pixels wide,
	and the X register sets the horizontal position of the middle of that sprite.

	Render the image given by your program. What eight capital letters appear on your CRT?
	'''

	PART = 2

	def run(self):
		runner = self.device.run(tail_noop=True)
		next(runner)
		final_answer = []

		for y in range(6):
			new_line = []

			for x in range(40):
				new_line.append(self.device.sprite_visible(x,y))
				next(runner)

			final_answer.append(new_line)

			yield ''.join('█' if x else '░' for x in new_line)

		yield encode_lines(final_answer, concrete=True)



class CommDevice(CPU):
	def __init__(self):
		super().__init__()

		self.registers['X'] = 1

	@property
	def signal_strength(self):
		return (self.time+1) * self.registers['X']

	def sprite_visible(self, x, y):
		'''
		The sprite is 3 pixels wide, and the X register sets the horizontal position of the middle of that sprite.

		Takes y parameter as future-proofing and for aesthetics.
		'''

		return (self.registers['X'] - 1) <= x <= (self.registers['X'] + 1)

@CommDevice.register()
class NoOp(Instruction):
	NAME = 'noop'
	TARGET = None

	def __repr__(self):
		return 'noop'

@CommDevice.register()
class AddX(Instruction):
	NAME = 'addx'
	TARGET = 'X'
	DELAY = 2

	def __init__(self, cpu, amt_str):
		super().__init__(cpu)

		self.amount = int(amt_str)

	def __repr__(self):
		return f'addx {self.amount}'

	@property
	def result(self):
		return self.cpu.registers['X'] + self.amount
