from enum import IntEnum

from lib.main import DayRunner
from lib.package import register


class Signal: pass
class Sentinel: pass

class Shape(IntEnum):
	A = X = ROCK = 1
	B = Y = PAPER = 2
	C = Z = SCISSORS = 3

class Outcome(IntEnum):
	X = LOSS = 0
	Y = DRAW = 3
	Z = WIN = 6

OUTCOME_MAP = {
	(Shape.ROCK, Shape.ROCK): Outcome.DRAW,
	(Shape.PAPER, Shape.PAPER): Outcome.DRAW,
	(Shape.SCISSORS, Shape.SCISSORS): Outcome.DRAW,

	(Shape.ROCK, Shape.PAPER): Outcome.WIN,
	(Shape.PAPER, Shape.SCISSORS): Outcome.WIN,
	(Shape.SCISSORS, Shape.ROCK): Outcome.WIN,

	(Shape.PAPER, Shape.ROCK): Outcome.LOSS,
	(Shape.SCISSORS, Shape.PAPER): Outcome.LOSS,
	(Shape.ROCK, Shape.SCISSORS): Outcome.LOSS,
}
MY_SHAPE_MAP = {(h,o): m for (h,m),o in OUTCOME_MAP.items()}


class Day2(DayRunner):
	'''
	Score calculated as follows:
		  your throw value
		+ win (6)/tie (3)/lose (0)
	'''

	DAY = 2

	@classmethod
	def parse(cls, buffer):
		for line in buffer:
			line = line.strip()

			if line:
				yield line.split(' ')

	def munge_in(self, parsed_data):
		self.playbook = [(Shape[his], self.intepret_other(other)) for his,other in parsed_data]

@register(DayRunner)
class Part1(Day2):
	'''
	What would your total score be if everything goes exactly according to your strategy guide?
	'''

	PART = 1

	def run(self):
		score = 0

		yield len(self.playbook)

		for his, mine in self.playbook:
			outcome = OUTCOME_MAP[his,mine]
			round_score = mine + outcome
			score += round_score

		yield score

	def intepret_other(self, other):
		return Shape[other]

@register(DayRunner)
class Part2(Day2):
	'''
	Following the Elf's instructions for the second column, what would your total score be if
	everything goes exactly according to your strategy guide?

	"Anyway, the second column says how the round needs to end: X means you need to lose, Y means
	you need to end the round in a draw, and Z means you need to win. Good luck!"
	'''

	PART = 2

	def run(self):
		score = 0

		for his,outcome in self.playbook:
			mine = MY_SHAPE_MAP[his,outcome]
			round_score = mine + outcome
			score += round_score

		yield score

	def intepret_other(self, other):
		return Outcome[other]
