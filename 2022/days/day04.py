import re

from lib.main import DayRunner
from lib.package import register
from lib.range import Range


class Signal: pass
class Sentinel: pass


class Day4(DayRunner):
	'''
	'''

	DAY = 4

	@classmethod
	def parse_line(cls, line):
		match = re.fullmatch(r'(\d+)-(\d+),(\d+)-(\d+)', line)

		yield Range(int(match.group(1)), int(match.group(2))), Range(int(match.group(3)), int(match.group(4)))

	def munge_in(self, parsed_data):
		self.pairs = list(parsed_data)

@register(DayRunner)
class Part1(Day4):
	'''
	In how many assignment pairs does one range fully contain the other?
	'''

	PART = 1

	def run(self):
		yield sum(a >= b or b >= a for a,b in self.pairs)


@register(DayRunner)
class Part2(Day4):
	'''
	In how many assignment pairs do the ranges overlap?
	'''

	PART = 2

	def run(self):
		yield sum(bool(a & b) for a,b in self.pairs)
