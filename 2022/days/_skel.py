from lib.main import DayRunner
from lib.package import register


class Signal(Exception): pass

class Error(Exception): pass

class Sentinel: pass


class Day4(DayRunner):
	'''
	'''

	DAY = 4

	def munge_in(self, parsed_data):
		pass

@register(DayRunner)
class Part1(Day4):
	'''
	'''

	PART = 1

	def run(self):
		pass


@register(DayRunner)
class Part2(Day4):
	'''
	'''

	PART = 2

	def run(self):
		pass
