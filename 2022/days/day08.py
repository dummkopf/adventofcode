from functools import lru_cache

from lib.grid import Grid
from lib.main import DayRunner
from lib.package import register
from lib.vector import Vector


class Signal(Exception): pass

class Sentinel: pass

class Error(Exception): pass


class Day8(DayRunner):
	'''
	'''

	DAY = 8

	@classmethod
	def parse_line(cls, line):
		yield map(int, line)

	def munge_in(self, parsed_data):
		self.grid = TreeGrid(parsed_data)

@register(DayRunner)
class Part1(Day8):
	'''
	Consider your map; how many trees are visible from outside the grid?
	'''

	PART = 1

	def run(self):
		yield sum(self.grid.is_visible(pos) for pos in self.grid.keys())


@register(DayRunner)
class Part2(Day8):
	'''
	Consider each tree on your map. What is the highest scenic score possible for any tree?
	'''

	PART = 2

	def run(self):
		yield max(self.grid.scenic_score(pos) for pos in self.grid.keys())


class TreeGrid(Grid):
	def is_visible(self, where):
		'''
		A tree is visible if all of the other trees between it and an edge of the grid are shorter than it.
		Only consider trees in the same row or column; that is, only look up, down, left, or right from any given tree.
		'''

		# NOTE: could be optimized: e.g. if a tree is taller than all on its right, each tree on its right knows that it's not the tallest to its left
		#       but the puzzle input is just a 100x100, so eh

		target_height = self[where]
		ortho_rays = [
			self.get_ray(where, Vector(0,1)),
			self.get_ray(where, Vector(0,-1)),
			self.get_ray(where, Vector(1,0)),
			self.get_ray(where, Vector(-1,0)),
		]

		return any(all(target_height > x for x in ray) for ray in ortho_rays)

	def scenic_score(self, where):
		'''
		To measure the viewing distance from a given tree, look up, down, left, and right from that tree; stop if you
		reach an edge or at the first tree that is the same height or taller than the tree under consideration.
		(If a tree is right on the edge, at least one of its viewing distances will be zero.)
		'''

		# extra optimization can be done by stopping if any result is zero, as the total result would then be known to be zero

		directions = [
			Vector(0,1),
			Vector(0,-1),
			Vector(1,0),
			Vector(-1,0),
		]
		product = 1

		for direction in directions:
			multiplicand = self.viewing_distance(where, direction)

			if multiplicand:
				product *= multiplicand
			else:
				return 0

		return product

	def viewing_distance(self, where, direction, target=None):
		'''
		Count number of trees in a ray that are shorter than `where`.
		Use recursion to simplify logic.
		'''

		target = target or self[where]
		neighbor = where + direction

		if neighbor[0] < 0 or neighbor[1] < 0 or neighbor[0] > self.maxx or neighbor[1] > self.maxy:
			return 0
		elif self[neighbor] >= target:
			return 1
		else:
			return self.viewing_distance(neighbor, direction, target=target) + 1
