from lib.main import DayRunner
from lib.package import register


class Signal(Exception): pass
class Sentinel: pass
class Error(Exception): pass

class Invalid(Signal): pass
class EOF(Signal): pass

class YouGoofed(Error): pass


class Day7(DayRunner):
	'''
	'''

	DAY = 7

	@classmethod
	def parse(cls, buffer):
		return buffer

	def munge_in(self, parsed_data):
		parser = Parser(parsed_data)
		parser.parse()
		self.root = parser.traverse()


@register(DayRunner)
class Part1(Day7):
	'''
	Find all of the directories with a total size of at most 100000. What is the sum of the total sizes of those directories?
	'''

	PART = 1

	def run(self):
		eligible_dirs = [x for x in self.root.walk_dirs() if x.size <= 100000]

		yield sum(x.size for x in eligible_dirs)


@register(DayRunner)
class Part2(Day7):
	'''
	Find the smallest directory that, if deleted, would free up enough space on the filesystem to run the update. What is the total size of that directory?
	'''

	PART = 2
	FS_SIZE = 70000000
	NEEDED_FREE = 30000000

	def run(self):
		current_free_space = self.FS_SIZE - self.root.size
		target_freed_space = self.NEEDED_FREE - current_free_space
		eligible_dirs = filter(lambda x: x.size >= target_freed_space, self.root.walk_dirs())
		sorted_eligible_dirs = sorted(eligible_dirs, key=lambda x: x.size)

		yield sorted_eligible_dirs[0].size


class Parser:
	def __init__(self, buffer):
		self.source = buffer
		self.symbols = []

	def parse(self):
		while True:
			try:
				self.parse_next()
			except EOF:
				break

	def parse_next(self):
		line = self.source.readline().rstrip()

		if line.startswith('$ cd '):
			self.symbols.extend(self.parse_cd(line[5:]))
		elif line.startswith('$ ls'):
			self.symbols.extend(self.parse_ls(line[5:]))
		elif not line:
			raise EOF
		else:
			raise YouGoofed

	def parse_cd(self, rest):
		if rest == '/':
			yield CdRoot()
		elif rest == '..':
			yield CdUp()
		else:
			yield CdDown(rest)

	def parse_ls(self, rest):
		yield ListDir()

		while True:
			start_pos = self.source.tell()

			try:
				yield self.parse_result()
			except Invalid:
				# trust that parse_result only reads one before deciding
				self.source.seek(start_pos)
				break
			except EOF:
				break

	def parse_result(self):
		first = self.source.read(1)

		if first == '$':
			raise Invalid
		elif first == '':
			raise EOF
		else:
			line = first + self.source.readline().strip()

			size_or_dir, name = line.split(' ')

			if size_or_dir == 'dir':
				return DirResult(name)
			else:
				size = int(size_or_dir)

				return FileResult(size, name)

	def traverse(self):
		root = RootDir()
		traverser = Traverser(root)

		for symbol in self.symbols:
			symbol.execute(traverser)

		return root


class Symbol:
	def execute(self, state):
		pass

class Operation(Symbol): pass
class ChDir(Operation):
	def execute(self, state):
		state.result_target = None

class CdRoot(ChDir):
	def execute(self, state):
		super().execute(state)
		state.current = state.current.root

class CdUp(ChDir):
	def execute(self, state):
		super().execute(state)
		state.move_up()

class CdDown(ChDir):
	def __init__(self, name):
		self.name = name

	def execute(self, state):
		super().execute(state)
		state.move_down(self.name)

class ListDir(Operation):
	def execute(self, state):
		state.result_target = state.current

class Result(Symbol): pass

class FileResult(Result):
	def __init__(self, size, name):
		self.size = size
		self.name = name

	def execute(self, state):
		state.result_target.add_file(self.name, self.size)

class DirResult(Result):
	def __init__(self, name):
		self.name = name

	def execute(self, state):
		state.result_target.add_dir(self.name)


class FsObj:
	def __init__(self, name, parent=None):
		self.name = name
		self.parent = parent

	@property
	def root(self):
		if self.parent is None:
			return self
		else:
			return self.parent.root

class BaseDir(FsObj):
	def __init__(self, name, parent):
		super().__init__(name, parent=parent)

		self.children = {}

	def add_dir(self, name):
		self.children[name] = Dir(name, parent=self)

	def add_file(self, name, size):
		self.children[name] = File(name, size, parent=self)

	@property
	def size(self):
		return sum(x.size for x in self.children.values())

	def walk_dirs(self):
		yield self

		for child in self.children.values():
			if isinstance(child, BaseDir):
				yield from child.walk_dirs()

class Dir(BaseDir):
	def __init__(self, name, parent):
		assert name is not None
		assert parent is not None

		super().__init__(name, parent)

class RootDir(BaseDir):
	def __init__(self):
		super().__init__(None, None)

class File(FsObj):
	def __init__(self, name, size, parent=None):
		assert name is not None

		super().__init__(name, parent=parent)

		self.size = size


class Traverser:
	def __init__(self, tree):
		self.current = tree
		self.await_results = None

	def move_up(self):
		self.current = self.current.parent

	def move_down(self, name):
		if not name in self.current.children:
			self.current.add_dir(name)

		self.current = self.current.children[name]
