from enum import IntEnum
from functools import reduce
import operator as op

from lib.main import DayRunner
from lib.package import register


class Signal: pass
class Sentinel: pass


class Day3(DayRunner):
	'''
	'''

	DAY = 3

	def munge_in(self, parsed_data):
		self.rucksacks = list(parsed_data)

	@classmethod
	def prioritize(cls, value):
		if 'a' <= value <= 'z':
			return ord(value) - ord('a') + 1
		elif 'A' <= value <= 'Z':
			return ord(value) - ord('A') + 27
		else:
			raise ValueError(value)

@register(DayRunner)
class Part1(Day3):
	'''
	Find the item type that appears in both compartments of each rucksack. What is the sum of the priorities of those item types?
	'''

	PART = 1

	def run(self):
		collisions = [self.find_collision(items[:len(items)//2],items[len(items)//2:]) for items in self.rucksacks]

		yield sum(self.prioritize(value) for value in collisions)

	@classmethod
	def find_collision(cls, left, right):
		collisions = set(left) & set(right)
		assert len(collisions) == 1
		return collisions.pop()


@register(DayRunner)
class Part2(Day3):
	'''
	Find the item type that corresponds to the badges of each three-Elf group. What is the sum of the priorities of those item types?
	'''

	PART = 2

	def run(self):
		badges = [self.find_badge(self.rucksacks[i:i+3]) for i in range(0,len(self.rucksacks),3)]

		yield sum(self.prioritize(value) for value in badges)

	@classmethod
	def find_badge(cls, rucksacks):
		return reduce(op.and_, map(set, rucksacks)).pop()
