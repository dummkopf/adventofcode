from collections import deque
import re

from lib.main import DayRunner
from lib.package import register


class Error(Exception): pass
class Signal: pass
class Sentinel: pass

class YouGoofed(Error): pass


class Day5(DayRunner):
	'''
	'''

	DAY = 5

	@classmethod
	def parse(cls, buffer):
		'''
		Real funky format here.
		Two sections: stacks and moves, blank line between
		'''

		# parse_stacks eats until and including blank separator
		stack_data = cls.parse_stacks(buffer)

		# move data is much nicer
		move_data = cls.parse_moves(buffer)

		return stack_data, move_data

	@classmethod
	def parse_stacks(cls, buffer):
		# fucking pivot

		# format:
		#  arranged in columns 3 wide with 1 space between (fencepost)
		#  last line: stack indexes
		#  other lines: stack contents

		raw_data = []
		indexed_data = None

		for line in buffer:
			if not line.strip():
				if not indexed_data:
					raise YouGoofed

				return indexed_data

			# don't full strip because blank spaces are significant
			if line.endswith('\n'):
				line = line[:-1]

			if len(line) >= 3: # one record
				if '[' in line:
					line_data = [None if line[1]==' ' else line[1]]

					# ternary isn't the cleanest, but eh
					# jump by 4 because fencepost (first is only of width 3)
					# fragile due to assumption that stack item id is len 1
					line_data.extend(None if line[i]==' ' else line[i] for i in range(5,len(line),4))
					raw_data.append(line_data)
				else:
					# no empty records here
					stack_names = line.split()
					indexed_data = {k: [r[i] for r in raw_data] for i,k in enumerate(stack_names)}
		else:
			raise YouGoofed

	@classmethod
	def parse_moves(cls, buffer):
		# format: move 1 from 2 to 1

		for line in buffer:
			if line.strip():
				match = re.match(r'move (?P<qty>\d+) from (?P<from>\d+) to (?P<to>\d+)', line)

				yield int(match.group('qty')), match.group('from'), match.group('to')

	def munge_in(self, parsed_data):
		stack_data, move_data = parsed_data

		self.munge_stacks(stack_data)
		self.munge_moves(move_data)

	def munge_stacks(self, stack_data):
		self.stacks = {k: deque(reversed([x for x in v if x])) for k,v in stack_data.items()}

	def munge_moves(self, move_data):
		self.moves = [Move(*x) for x in move_data]

	@property
	def stack_tops(self):
		ordered_stacks = [v for k,v in sorted(self.stacks.items())]

		return [s[-1] if s else None for s in ordered_stacks]

@register(DayRunner)
class Part1(Day5):
	'''
	After the rearrangement procedure completes, what crate ends up on top of each stack?
	'''

	PART = 1

	def run(self):
		for move in self.moves:
			yield str(move)
			move.run(self.stacks)

		yield ''.join(self.stack_tops)


@register(DayRunner)
class Part2(Day5):
	'''
	After the rearrangement procedure completes, what crate ends up on top of each stack?
	'''

	PART = 2

	def run(self):
		for move in self.moves:
			yield str(move)
			move.run(self.stacks, mode=9001)

		yield ''.join(self.stack_tops)


class Move:
	def __init__(self, quantity, source, destination):
		self.quantity = quantity
		self.source = source
		self.destination = destination

	def __str__(self):
		return f'<{self.__class__.__name__} {self.source} -> {self.destination} ({self.quantity})>'
	__repr__=__str__

	def run(self, stacks, mode=9000):
		addstack = [stacks[self.source].pop() for _ in range(self.quantity)]

		if mode==9000:
			pass # intentional
		elif mode==9001:
			addstack.reverse()
		else:
			raise YouGoofed

		stacks[self.destination].extend(addstack)
