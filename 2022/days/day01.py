from lib.main import DayRunner
from lib.package import register


class Signal: pass
class Sentinel: pass
class Blank(Sentinel): pass


class Day1(DayRunner):
	DAY = 1

	@classmethod
	def parse(cls, buffer):
		for line in buffer:
			line = line.strip()

			if line:
				yield int(line)
			else:
				yield Blank

		yield Blank

	def munge_in(self, parsed_data):
		self.calorie_counts = []
		running_count = 0

		for value in parsed_data:
			if value is Blank:
				self.calorie_counts.append(running_count)
				running_count = 0
			else:
				running_count += value

@register(DayRunner)
class Part1(Day1):
	'''
	Find the Elf carrying the most Calories. How many total Calories is that Elf carrying?
	'''

	PART = 1

	def run(self):
		yield max(self.calorie_counts)

@register(DayRunner)
class Part2(Day1):
	'''
	Find the top three Elves carrying the most Calories. How many Calories are those Elves carrying in total?
	'''

	PART = 2

	def run(self):
		sorted_counts = sorted(self.calorie_counts, reverse=True)

		yield sorted_counts
		yield sum(sorted_counts[:3])
