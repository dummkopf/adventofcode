from itertools import repeat

from lib.main import DayRunner
from lib.package import register
from lib.vector import Vector


class Signal(Exception): pass

class Error(Exception): pass

class Sentinel: pass


class Day9(DayRunner):
	'''
	'''

	DAY = 9

	@classmethod
	def parse_line(cls, line):
		yield Move.from_line(line)

	def munge_in(self, parsed_data):
		self.bridge = Bridge(Itinerary(parsed_data), self.BRIDGE_LENGTH)

	def run(self):
		if self.DEBUG:
			traversed = set()
			oldt = Vector(0,0)

			for h,t in self.bridge.walk():
				traversed.add(t)

				if oldt == t:
					movement_indicator = 'o'
				elif (t-oldt).is_ortho:
					movement_indicator = '|'
				else:
					movement_indicator = '/'

				yield '{} {} {}'.format(movement_indicator, ','.join(map(str, h)), ','.join(map(str, t)))
				oldt = t

			yield len(traversed)
		else:
			yield len(set(t for h,t in self.bridge.walk()))

@register(DayRunner)
class Part1(Day9):
	'''
	Simulate your complete hypothetical series of motions. How many positions does the tail of the rope visit at least once?
	'''

	PART = 1
	BRIDGE_LENGTH = 2

@register(DayRunner)
class Part2(Day9):
	'''
	Simulate your complete series of motions on a larger rope with ten knots. How many positions does the tail of the rope visit at least once?
	'''

	PART = 2
	BRIDGE_LENGTH = 10


class Itinerary:
	def __init__(self, steps):
		self.steps = list(steps)

	@classmethod
	def from_buffer(cls, buffer):
		for line in buffer:
			yield Move.from_line(line.strip())

	def __iter__(self):
		for step in self.steps:
			yield from iter(step)

	def __str__(self):
		return ','.join(map(str, self.steps))

	def __repr__(self):
		return f'<{self.__class__.__name__} {self!s}>'

class Move:
	DIRECTION_MAP = {
		'L': Vector(-1, 0),
		'R': Vector(1, 0),
		'U': Vector(0,1),
		'D': Vector(0,-1),
	}
	DIRECTION_MAP['_reversed'] = {v:k for k,v in DIRECTION_MAP.items()}

	def __init__(self, direction, distance):
		self.direction = direction
		self.distance = distance

	def __str__(self):
		return f'{self.DIRECTION_MAP["_reversed"][self.direction]}{self.distance:d}'

	def __repr__(self):
		return f'<{self.__class__.__name__} {self!s}>'

	@classmethod
	def from_line(cls, line):
		dir_str, dist_str = line.split(' ')

		direction = cls.DIRECTION_MAP[dir_str]
		distance = int(dist_str)

		assert distance >= 0

		return cls(direction, distance)

	def __iter__(self):
		return repeat(self.direction, self.distance)

class Bridge:
	def __init__(self, itinerary, n=2):
		self.itinerary = itinerary

		self.reset(n)

	def walk(self):
		yield self.head.position, self.tail.position

		for step in self.itinerary:
			self.head.move(step)

			yield self.head.position, self.tail.position

	def reset(self, n):
		self.tail = self.head = Knot()

		for i in range(n-1):
			self.head = Knot(self.head)


class Knot:
	def __init__(self, next=None):
		self.next = next
		self.position = Vector(0,0)

	def move(self, delta):
		self.position += delta
		self.move_next()

	def move_next(self):
		'''
		If the head is ever two steps directly up, down, left, or right from the tail, the tail must also move one step in that direction so it remains close enough

		Otherwise, if the head and tail aren't touching and aren't in the same row or column, the tail always moves one step diagonally to keep up
		'''

		if self.next and not self.is_touching:
			self.next.move(self.headward.cardinal_direction)

			###DEBUG
			assert self.is_touching

	@property
	def headward(self):
		return self.position - self.next.position

	@property
	def is_touching(self):
		'''
		If overlapping or adjacent orthogonally or diagonally
		'''

		return self.headward.ring_distance < 2
