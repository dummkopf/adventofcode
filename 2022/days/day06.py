from collections import deque

from lib.main import DayRunner
from lib.package import register


class Signal: pass
class Sentinel: pass


class Day6(DayRunner):
	'''
	'''

	DAY = 6
	WINDOW_SIZE = None

	@classmethod
	def parse(cls, buffer):
		return buffer

	def munge_in(self, parsed_data):
		self.data_queue = Queue(parsed_data, self.WINDOW_SIZE, initial_eat=True)

	def run(self):
		yield from self.unique_string_end

	@property
	def unique_string_end(self):
		while not self.data_queue.is_unique:
			self.data_queue.eat()

			if self.DEBUG:
				yield ''.join(self.data_queue.buffer)

		yield self.data_queue.source.tell()

@register(DayRunner)
class Part1(Day6):
	'''
	How many characters need to be processed before the first start-of-packet marker is detected?
	'''

	PART = 1
	WINDOW_SIZE = 4

@register(DayRunner)
class Part2(Day6):
	'''
	How many characters need to be processed before the first start-of-message marker is detected?
	'''

	PART = 2
	WINDOW_SIZE = 14


class Queue:
	def __init__(self, source, size, initial_eat=False):
		self.source = source
		self.buffer = deque(maxlen=size)

		if initial_eat:
			self.eat(size)

	def eat(self, n=1):
		self.buffer.extend(self.source.read(n))

	@property
	def is_unique(self):
		return len(set(self.buffer)) == self.buffer.maxlen
