from collections import deque
from math import prod
import operator as op
import re

from lib.main import DayRunner
from lib.mod import egcd
from lib.package import register


class Signal(Exception): pass

class Error(Exception): pass

class Sentinel: pass


class Day11(DayRunner):
	'''
	Monkeys are playing Keep Away with your missing things!

	To get your stuff back, you need to be able to predict where the monkeys will throw your items.
	After some careful observation, you realize the monkeys operate based on how worried you are
	about each item.
	'''

	DAY = 11
	RELIEF = None
	ROUNDS = None
	REPORT_INTERVAL = None


	@classmethod
	def parse(cls, buffer):
		for line in buffer:
			line = line.rstrip('\n')

			yield from cls.parse_line(line)

	def munge_in(self, parsed_data):
		monkeys = []

		while True:
			monkeys.append(Monkey.parse(parsed_data))

			try:
				line = next(parsed_data)
				match = re.fullmatch(r'\s*', line)
			except StopIteration:
				break
			else:
				assert match

		self.barrel = Barrel(monkeys)

	def run(self):
		for r in range(self.ROUNDS):
			self.barrel.run_round(relief=self.RELIEF)

			if self.DEBUG and (r+1) % self.REPORT_INTERVAL == 0:
				yield f'== After round {r+1} =='
				yield from self.barrel.report
				yield ''

		yield prod(sorted((m.inspections for m in self.barrel.monkeys), reverse=True)[:2])


@register(DayRunner)
class Part1(Day11):
	'''
	Figure out which monkeys to chase by counting how many items they inspect over 20 rounds.
	What is the level of monkey business after 20 rounds of stuff-slinging simian shenanigans?
	'''

	PART = 1
	RELIEF = 3
	ROUNDS = 20
	REPORT_INTERVAL = 1


@register(DayRunner)
class Part2(Day11):
	'''
	Worry levels are no longer divided by three after each item is inspected;
	you'll need to find another way to keep your worry levels manageable.
	Starting again from the initial state in your puzzle input,
	what is the level of monkey business after 10000 rounds?
	'''

	PART = 2
	RELIEF = 1
	ROUNDS = 10000
	REPORT_INTERVAL = 1000


class Barrel:
	'''
	Container for monkeys.
	'''

	def __init__(self, monkeys):
		self.monkeys = []

		for monkey in monkeys:
			monkey.barrel = self
			self.monkeys.append(monkey)

		# presume test_mod is always prime
		self.lcm = prod(x.test_mod for x in self.monkeys)

	def run_round(self, relief):
		for monkey in self.monkeys:
			monkey.run_turn(relief=relief)

	@property
	def report(self):
		for monkey in self.monkeys:
			yield str(monkey)

class Monkey:
	def __init__(self, index, items, operator, operand, test_mod, true_target, false_target):
		self.index = index
		self.items = deque(x%test_mod for x in items)
		self.operator = operator
		self.operand = operand
		self.test_mod = test_mod
		self.true_target = true_target
		self.false_target = false_target

		self.barrel = None
		self.inspections = 0

	def __str__(self):
		return f'Monkey {self.index}: {", ".join(map(str, self.items))}'

	def run_turn(self, relief):
		self.inspections += len(self.items)

		while self.items:
			item = self.items.popleft()

			# TODO: get the n-wise Bezout coefs
			#       when doing the Operation, update all congruencies (recalc x,y,g)
			#       need to store x and y
			#       no need to store entire operation history, as p(a) ≡ p(b) (modn)
			new_item = (self.operator(item, self.operand) // relief) % self.barrel.lcm
			target_index = self.true_target if new_item % self.test_mod == 0 else self.false_target
			target = self.barrel.monkeys[target_index]

			target.items.append(new_item)

	@classmethod
	def parse(cls, line_iter):
		index = cls.parse_header(next(line_iter))
		items = cls.parse_starting_items(next(line_iter))
		operator,operand = cls.parse_operation(next(line_iter))
		test_mod = cls.parse_test(next(line_iter))
		true_target = cls.parse_true(next(line_iter))
		false_target = cls.parse_false(next(line_iter))

		return cls(index, items, operator, operand, test_mod, true_target, false_target)

	@classmethod
	def parse_header(cls, line):
		match = re.fullmatch(r'Monkey (?P<index>\d+):', line)
		assert match

		return int(match.group('index'))

	@classmethod
	def parse_starting_items(cls, line):
		match = re.fullmatch(r'  Starting items: (?P<items>(\d+)(?:, (\d+))*)?', line)
		assert match

		return [int(x) for x in match.group('items').split(', ')]

	@classmethod
	def parse_operation(cls, line):
		match = re.fullmatch(r'  Operation: new = old (?P<op>[\*\+]) (?P<val>old|\d+)', line)
		assert match

		if match.group('op') == '*':
			operator = op.mul
		elif match.group('op') == '+':
			operator = op.add
		else:
			raise ParseError

		if match.group('val') == 'old':
			if operator is op.add:
				operator = op.mul
			elif operator is op.mul:
				operator = op.pow

			operand = 2
		else:
			operand = int(match.group('val'))

		return operator, operand

	@classmethod
	def parse_test(cls, line):
		match = re.fullmatch(r'  Test: divisible by (?P<mod>\d+)', line)
		assert match

		return int(match.group('mod'))

	@classmethod
	def parse_true(cls, line):
		match = re.fullmatch(r'    If true: throw to monkey (?P<target>\d+)', line)
		assert match

		return int(match.group('target'))

	@classmethod
	def parse_false(cls, line):
		match = re.fullmatch(r'    If false: throw to monkey (?P<target>\d+)', line)
		assert match

		return int(match.group('target'))
